rmdir /S /Q build
mkdir build
cd build 
mkdir release
mkdir debug
cd ..

MSBuild.exe D2MapGenerator.sln /t:Clean,Build /p:Configuration=Release
MSBuild.exe D2MapGenerator.sln /t:Clean,Build /p:Configuration=Debug

xcopy %cd%\StackGen\Resources\* build\release\Resources /R /I /Y /S
xcopy %cd%\StackGen\RandomStackGenerator.dll build\release /R /I /Y /S
xcopy %cd%\StackGen\RandomStackGenerator.xml build\release /R /I /Y /S

xcopy %cd%\StackGen\Resources\* build\debug\Resources /R /I /Y /S
xcopy %cd%\StackGen\RandomStackGenerator.dll build\debug /R /I /Y /S

xcopy D2MapGenerator\bin\Release\D2MapEdit.exe build\release /R /I /Y /S
xcopy D2MapGenerator\bin\Release\*.tr build\release /R /I /Y /S
xcopy D2MapGenerator\bin\Debug\D2MapEdit.exe build\debug /R /I /Y /S
xcopy D2MapGenerator\bin\Debug\*.tr build\debug /R /I /Y /S

cd build/release
"C:\Program Files\7-Zip\7z.exe" a -tzip -mx5 -r0 ../NevendaarTools_release.zip
cd..
cd debug
"C:\Program Files\7-Zip\7z.exe" a -tzip -mx5 -r0 ../NevendaarTools_debug.zip
cd ../..