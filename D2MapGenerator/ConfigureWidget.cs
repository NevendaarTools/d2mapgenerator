﻿using NevendaarTools;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;
using NevendaarTools.Transform;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace D2MapEdit
{
    public partial class ConfigureWidget : UserControl
    {
        SettingsManager settings = SettingsManager.Instance("settings.xml");
        private MapReader mapReader = new MapReader();
        MapModel mapModel = new MapModel();
        MapEditor editor = new MapEditor();
        MapOptionsModel optionsModel;
        OptionHelper optionHelper = new OptionHelper();
        public GameModel model;

        string filename;
        string dir;

        private void Log(string str)
        {
            logTextBox.AppendText(str + "\n");
        }

        public void ReloadTranslation()
        {
            TranslationHelper.Instance().LoadExtra(dir);
            optionHelper.Reload();
            buttonOpen.Text = TranslationHelper.Instance().Tr("Open map");
            buttonApply.Text = TranslationHelper.Instance().Tr("Apply");
            buttonCreate.Text = TranslationHelper.Instance().Tr("Create for map");
        }

        public ConfigureWidget()
        {
            InitializeComponent();
            optionsModel = new MapOptionsModel(editor);
            optionsModel._LogDelegate += Log;
            ReloadTranslation();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файлы карт|*.sg;";
            openDialog.InitialDirectory = settings.Value("openPath", ".");
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;
            filename = openDialog.FileName;
            mapReader.ReadMap(mapModel, openDialog.FileName);
            mapInfoWidget1.SetMap(ref mapModel, model);
            dir = filename.Replace(".sg", ".settings");

            TranslationHelper.Instance().LoadExtra(dir);

            if (optionsModel.ReadOptions(ref mapModel, dir))
            {
                optionHelper.Init(optionsModel, flowLayoutPanel1);
                optionHelper.Reload();
                buttonCreate.Enabled = false;
            }
            else
            {
                Log("Failed to find settings for map(" + filename.Replace(".sg", ".settings") + "/options.json)");
                buttonCreate.Enabled = true;
                optionsModel.Clear();
                optionHelper.Reload();
            }
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            mapInfoWidget1.Apply();
            if (MapReader.SaveMap(mapModel, filename))
            {
                Log("New settings applied to map");
            }
            else
            {
                Log("Failed to save map");
            }
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            D2SceneVariables variables = mapModel.GetById<D2SceneVariables>("SV0000");
            JSONObject json = new JSONObject();
            JSONObject preset = new JSONObject();
            SimpleJSON.JSONArray presetOptions = new JSONArray();

            SimpleJSON.JSONArray groups = new JSONArray();
            JSONObject group = new JSONObject();
            SimpleJSON.JSONArray options = new JSONArray();

            for (int i = 0; i < 6; ++i)
            {
                string name = "TIER_" + i.ToString() + "_CITY_INCOME";
                if (editor.GetVariable(ref mapModel, name) == null)
                {
                    variables._Variables.Add(new D2SceneVariables.SceneVariable()
                    {
                        _Id = variables._Variables.Count,
                        _Name = name,
                        _Value = 0
                    });
                }
            }
            List<string> lines = new List<string>();
            for (int i = 0; i < variables._Variables.Count; ++i)
            {
                D2SceneVariables.SceneVariable variable = variables._Variables[i];
                Option option = Option.IntOption(variable._Name, variable._Value, "");
                {
                    JSONObject presetValue = new JSONObject();
                    presetValue["group"] = "All";
                    presetValue["name"] = option.name;
                    presetValue["value"] = option.value;
                    presetOptions.Add(presetValue);

                    lines.Add(option.name + "==>");
                }
                options.Add(JsonHelper.OptionToJson(option));
                if (i %10 == 0 && i != 0 && i != variables._Variables.Count - 1)
                {
                    group["group"] = "All";
                    group["options"] = options;
                    groups.Add(group);
                    group = new JSONObject();
                    options = new JSONArray();
                }
            }
            Option test = Option.EnumOption("TestEnum", "var1", "test1_desc", 
                new List<string>() { "var1", "var2" },
                new List<string>() { "1", "2"}
                );
            options.Add(JsonHelper.OptionToJson(test));

            Option test2 = Option.BoolOption("TestBool", true, "test1_desc");
            options.Add(JsonHelper.OptionToJson(test2));

            mapModel.Replace(variables);
            group["group"] = "All";
            group["options"] = options;
            groups.Add(group);
            json["groups"] = groups;
            JsonHelper.Save(dir + "/options.json", json);

            preset["options"] = presetOptions;
            JsonHelper.Save(dir + "/preset_default.json", preset);

            System.IO.File.WriteAllLines(dir + "/ru.tr", lines);
            System.IO.File.WriteAllLines(dir + "/en.tr", lines);

            if (optionsModel.ReadOptions(ref mapModel, dir))
            {
                optionHelper.Init(optionsModel, flowLayoutPanel1);
                optionHelper.Reload();
                buttonCreate.Enabled = false;
            }
            else
            {
                Log("Failed to create settings for map(" + filename.Replace(".sg", ".settings") + "/options.json)");
                buttonCreate.Enabled = true;
                optionsModel.Clear();
                optionHelper.Reload();
            }
            if (MapReader.SaveMap(mapModel, filename))
            {
                Log("New settings applied to map");
            }
            else
            {
                Log("Failed to save map");
            }
        }

        private void ConfigureWidget_Load(object sender, EventArgs e)
        {
            flowLayoutPanel1.Height = Height - 30;
        }
    }
}
