﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NevendaarTools.DataTypes;
using NevendaarTools;
using NevendaarTools.MapUtils;

namespace NevedaarWidgets
{
    public partial class MapInfoWidget: UserControl
    {
        MapModel mapModel = null;
        GameModel model = null;
        ToolTip tt = new ToolTip();

        public MapInfoWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void ReloadTranslation()
        {
            labelAuthor.Text = TranslationHelper.Instance().Tr("Author:");
            labelName.Text = TranslationHelper.Instance().Tr("Name:");
            labelSizeTitle.Text = TranslationHelper.Instance().Tr("Size:");
            labelFormatTitle.Text = TranslationHelper.Instance().Tr("Format:");
            racesLabel.Text = TranslationHelper.Instance().Tr("Races:");
        }
        
        public void SetMap(ref MapModel map, GameModel model)
        {
            this.model = model;
            textBoxName.Text = map.Header._Name;
            textBoxAuthor.Text = map.Header._Author;
            labelFormat.Text = map.Header._MapType + " " + map.Header._Version;
            D2Plan plan = map.GetById<D2Plan>("PN0000");
            int mapSize = plan._mapSize;

            labelSize.Text = mapSize.ToString() + " x " + mapSize.ToString();
            pictureBox1.Image = MapImageHelper.MiniMap(ref map, ref model);
            mapModel = map;            
            racesPictureBox.Image = MapImageHelper.RaceListImage(ref map, ref model);
            List<D2Player> players = map.GetListByType<D2Player>();
            string raceList = "";
            foreach (D2Player player in players)
            {
                Grace race = model.GetObjectT<Grace>(player._raceId);
                if (!race.playable)
                    continue;
                raceList += race.name_txt.value.text;
                if (player._alwaysai)
                    raceList += " [AI]";
                raceList += "\n";
            }
            tt.SetToolTip(this.racesPictureBox, raceList);
        }

        public string MapName()
        {
            return textBoxName.Text;
        }

        public string MapAuthor()
        {
            return textBoxAuthor.Text;
        }

        public void SetMapName(string name)
        {
            textBoxName.Text = name;
            if (mapModel != null)
            {
                MapEditor.Rename(ref mapModel, MapName(), MapAuthor()); 
            }
        }

        public void SetMapAuthor(string author)
        {
            textBoxAuthor.Text = author;
            if (mapModel != null)
            {
                MapEditor.Rename(ref mapModel, MapName(), MapAuthor());
            }
        }

        public Image GetMapImage()
        {
            return pictureBox1.Image;
        }

        public void Apply()
        {
            if (mapModel == null)
                return;
            MapEditor.Rename(ref mapModel, MapName(), MapAuthor());
        }

        private void MapInfoWidget_Load(object sender, EventArgs e)
        {
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
            if (pictureBox1.Width < pictureBox1.Height)
            {
                pictureBox1.Height = pictureBox1.Width;
            }
            else
            {
                pictureBox1.Width = pictureBox1.Height;
            }
            int posX = Width - pictureBox1.Width;
            posX /= 2;
            pictureBox1.Location = new Point(posX, pictureBox1.Location.Y);
        }
    }
}
