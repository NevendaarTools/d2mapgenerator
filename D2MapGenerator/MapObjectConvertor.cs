﻿using System;
using System.Collections.Generic;
using System.Text;
using D2MapEdit;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;
using static NevendaarTools.DataTypes.D2Merchant;
using static NevendaarTools.DataTypes.D2Mercs;

namespace NevendaarTools.Transform
{
    public struct RuinReward
    {
        public string item;
        public string cash;
    }

    public interface MapObjectRegenerator
    {
        OptionsStorage Options();
        void Configure(MapEditor editor);
        StackTemplate RegenerateStack(ref StackTemplate template, float extraPowerMult = 1.0f, float extraItemsMult = 1.0f);
        RuinReward RegenerateRuinReward(string item, string cash, bool addItemCostDelta, int x, int y, float manaChance, float manaValue);
        List<MerchantItemEntry> RegenerateMerchantItem(ref List<MerchantItemEntry> old, int mode);
        List<MercsUnitEntry> RegenerateMercsUnits(ref List<MercsUnitEntry> old, int mode);

        string RegenerateItem(string item, int x, int y);
        string GenerateItem(string cost, int x, int y, float mod = 0.5f);
        List<string> RegenerateItems(ref List<string> items, int x, int y);
        void Finish(MapModel model);
    }

    public class DummyRegenerator : MapObjectRegenerator, OptionsStorage
    {
        public OptionsStorage Options() { return this; }
        public void Configure(MapEditor editor) { }
        public void Finish(MapModel model) { }
        public StackTemplate RegenerateStack(ref StackTemplate template, float extraPowerMult = 1.0f, float extraItemsMult = 1.0f) { return template; }
        public RuinReward RegenerateRuinReward(string item, string cash, bool addItemCostDelta, int x, int y, float manaChance, float manaValue) { return new RuinReward() { item = item, cash = cash }; }
        public List<MerchantItemEntry> RegenerateMerchantItem(ref List<MerchantItemEntry> old, int mode) { return old; }
        public List<MercsUnitEntry> RegenerateMercsUnits(ref List<MercsUnitEntry> old, int mode) { return old; }

        public string RegenerateItem(string item, int x, int y) { return item; }
        public string GenerateItem(string cost, int x, int y, float mod = 0.5f) { return "G000000000"; }
        public List<string> RegenerateItems(ref List<string> items, int x, int y) { return items; }
        public void Finish() { }

        public int OptionsCount()
        {
            return 0;
        }

        public int SetOption(int index, string value, ref bool neadReload)
        {
            return 0;
        }

        public Option GetOptionAt(int index)
        {
            return null;
        }
    }

    public class MapObjectconverter
    {
        public Dictionary<string, D2Item> _ItemMapping = new Dictionary<string, D2Item>();
        public Dictionary<string, D2Unit> _UnitMapping = new Dictionary<string, D2Unit>();

        public void Init(ref MapModel map)
        {
            _ItemMapping.Clear();
            _UnitMapping.Clear();

            List<D2Item> items = map.GetListByType<D2Item>();
            foreach (D2Item item in items)
            {
                _ItemMapping.Add(item._objId, item);
            }

            List<D2Unit> units = map.GetListByType<D2Unit>();
            foreach (D2Unit unit in units)
            {
                _UnitMapping.Add(unit._ObjId, unit);
            }
        }

        public StackTemplate ToStackTemplate(D2Stack stack)
        {
            StackTemplate template = new StackTemplate();
            template.order = stack._order;
            template.orderTarget = stack._orderTarg;
            template.aiOrder = stack._aiorder;
            template.aiOrderTarget = stack._aiordertar;
            D2Unit leader = _UnitMapping[stack._leaderId];
            template.name = leader._Name;

            for (int i = 0; i < 6; ++i)
            {
                template._Pos[i] = stack._Pos[i];
                if (stack._Unit[i] != "000000")
                {
                    if (stack._Unit[i] == stack._leaderId)
                        template._LeaderIndex = i;

                    D2Unit unit = _UnitMapping[stack._Unit[i]];
                    template._Unit[i] = unit._Type;
                    template._UnitLvl[i] = unit._Level;
                    foreach (string modId in unit._Mods)
                    {
                        template._Mods[i].Add(modId);
                    }
                }
            }
            foreach (string item in stack._items)
            {
                template._Items.Add(_ItemMapping[item]._type);
            }
            template.locationName = stack._posX.ToString() + "_" + stack._posY.ToString();
            template.mapX = stack._posX;
            template.mapY = stack._posY;

            return template;
        }

        public StackTemplate ToStackTemplate(D2StackTemplate stack)
        {
            StackTemplate template = new StackTemplate();
            template.template = true;
            template.order = stack._Order;
            template.orderTarget = stack._OrderTarg;
            template.name = stack._Name;

            for (int i = 0; i < 6; ++i)
            {
                template._Pos[i] = stack._Pos[i];
                template._Unit[i] = stack._Unit[i];
                template._UnitLvl[i] = stack._UnitLvl[i];

                if (stack._Unit[i] == stack._Leader)
                {
                    template._LeaderIndex = i;
                    template._UnitLvl[i] = stack._LeaderLvl;
                }
            }

            foreach (D2StackTemplate.UnitTemplateMod mod in stack._Mods)
                template._Mods[stack._Pos[mod._UnitNum]].Add(mod._Modid);

            template.locationName = "Template - " + stack._Name;

            return template;
        }

        public StackTemplate ToStackTemplate(D2Village village)
        {
            StackTemplate template = new StackTemplate();

            template.name = village._Name;
            template._LeaderIndex = -1;
            for (int i = 0; i < 6; ++i)
            {
                template._Pos[i] = village._Pos[i];
                if (village._Unit[i] != "000000")
                {
                    D2Unit unit = _UnitMapping[village._Unit[i]];
                    template._Unit[i] = unit._Type;
                    template._UnitLvl[i] = unit._Level;
                    foreach (string modId in unit._Mods)
                    {
                        template._Mods[i].Add(modId);
                    }
                }
            }
            foreach (string item in village._Items)
            {
                template._Items.Add(_ItemMapping[item]._type);
            }

            template.locationName = "Village - ";
            template.mapX = village._PosX;
            template.mapY = village._PosY;

            return template;
        }
        public StackTemplate ToStackTemplate(D2Ruin ruin)
        {
            StackTemplate template = new StackTemplate();

            template.template = true;
            template.name = ruin._name;
            template._LeaderIndex = -1;
            for (int i = 0; i < 6; ++i)
            {
                template._Pos[i] = ruin._pos[i];
                if (ruin._unit[i] != "000000")
                {
                    D2Unit unit = _UnitMapping[ruin._unit[i]];
                    template._Unit[i] = unit._Type;
                    template._UnitLvl[i] = unit._Level;
                    foreach (string modId in unit._Mods)
                    {
                        template._Mods[i].Add(modId);
                    }
                }
            }
            template.locationName = "Ruin - " + ruin._name;
            template.mapX = ruin._posX;
            template.mapY = ruin._posY;

            return template;
        }
    }
}
