﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;

namespace D2MapEdit
{
    public partial class MapGenerationWidget : UserControl
    {
        //MapGenAdapter _MapGenAdapter;
        OptionHelper optionHelper = new OptionHelper();
        private void Log(string str)
        {
            logTextBox.AppendText(str + "\n");
        }

        public void ReloadTranslation()
        {
            buttonGenerate.Text = TranslationHelper.Instance().Tr("Generate");
            buttonSaveGenMap.Text = TranslationHelper.Instance().Tr("Save");
            optionHelper.Reload();
        }
        public MapGenerationWidget()
        {
            InitializeComponent();
            ReloadTranslation();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void init(/*StackGenAdapter adapter*/)
        {
            //_MapGenAdapter = new MapGenAdapter();
            //_MapGenAdapter._mapCreator._LogDelegate += Log;
            //_MapGenAdapter._LogDelegate += Log;
            //_MapGenAdapter._Adapter = adapter;
            //buttonGenerate.Enabled = true;
            //_MapGenAdapter.PostInit();
            //optionHelper.Init(_MapGenAdapter, flowLayoutPanel2);
            //optionHelper.Reload();
            //if (mapInfoWidget1.MapName() == "")
            //    mapInfoWidget1.SetMapName(_MapGenAdapter._Settings.Value("map_name", "Generated map"));
            //if (mapInfoWidget1.MapAuthor() == "")
            //    mapInfoWidget1.SetMapAuthor(_MapGenAdapter._Settings.Value("author", "Generator"));
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            //Log("MapCreation started!");

            //_MapGenAdapter.Process();

            //buttonSaveGenMap.Enabled = true;

            //MapModificationHelper.Rename(ref _MapGenAdapter._mapCreator._map, 
            //    mapInfoWidget1.MapName(), 
            //    mapInfoWidget1.MapAuthor());
            //mapInfoWidget1.SetMap(ref _MapGenAdapter._mapCreator._map);
        }

        private void buttonSaveGenMap_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Файлы карт|*.sg;";
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            mapInfoWidget1.Apply();
            
            //_MapGenAdapter._Settings.SetValue("author", _MapGenAdapter._mapCreator._map.Header._Author);
            //_MapGenAdapter._Settings.SetValue("map_name", _MapGenAdapter._mapCreator._map.Header._Name);
            //_MapGenAdapter._mapCreator.SaveMap(saveFileDialog1.FileName);
        }
    }
}
