﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace NevendaarTools
{
    
    public class ExcludeSelectWidget
    {
        public delegate void LogDelegate(string text);
        public LogDelegate _LogDelegate;
        public void Log(string str)
        {
            _LogDelegate?.Invoke(str);
        }

        private PresetManager _Presets = new PresetManager();
        private List<string> _SelectedIds = new List<string>();
        TableLayoutPanel _Panel = null;
        FlowLayoutPanel _PresetsPanel = null;
        FlowLayoutPanel _GroupsPanel = null;
        FlowLayoutPanel _ItemsPanel = null;
        ListView _ExcludedView = null;
        TextBox _NameBox = null;

        List<string> _GroupNames = new List<string>();
        List<Button> _GroupsBindings = new List<Button>();
        private Dictionary<string, CheckBox> _Binding = new Dictionary<string, CheckBox>();

        private GameModel _GameModel;

        
        ToolTip _ToolTip1;

        Dictionary<string, PresetController> _PresetConstrollers = new Dictionary<string, PresetController>();
        List<string> EnabledPresetsIds(string exclude)
        {
            List<string> result = new List<string>();
            foreach(string key in _PresetConstrollers.Keys)
            {
                if (key == exclude)
                    continue;
                if (_PresetConstrollers[key].enabled)
                {
                    result.AddRange(_Presets._Presets[key].ids);
                }
            }
            return result;
        }

        public void UpdatePresetState(string name)
        {
            PresetController controller = _PresetConstrollers[name];
            if (!controller.enabled)
            {
                List<string> enabled = EnabledPresetsIds(name);
                foreach (string key in _Presets._Presets[name].ids)
                {
                    if (!enabled.Contains(key) && _SelectedIds.Contains(key))
                    {
                        _SelectedIds.Remove(key);
                    }
                }
            }
            else
            {
                foreach (string key in _Presets._Presets[name].ids)
                {
                    if (!_SelectedIds.Contains(key))
                    {
                        _SelectedIds.Add(key);
                    }
                }
            }
            UpdateExcludeList();
        }

        public void UpdateExcludeList()
        {
            _ExcludedView.Items.Clear();
            foreach (string id in _SelectedIds)
            {
                GItem item = _GameModel.GetObjectT<GItem>(id);
                if (item == null)
                    continue;
                ListViewItem item1WithToolTip = new ListViewItem(item.name_txt.value.text);
                item1WithToolTip.ToolTipText = item.desc_txt.value.text;
                _ExcludedView.Items.Add(item1WithToolTip);
            }
        }

        public ExcludeSelectWidget(GameModel gameModel)
        {
            _GameModel = gameModel;
            _ToolTip1 = new ToolTip();
            _ToolTip1.AutoPopDelay = 5000;
            _ToolTip1.InitialDelay = 400;
            _ToolTip1.ReshowDelay = 500;
            _Presets._LogDelegate += Log;
        }

        public bool PresetState(string name)
        {
            foreach (string key in _Presets._Presets[name].ids)
            {
                GItem item = _GameModel.GetObjectT<GItem>(key);
                if (item == null)
                    continue;
                if (!_SelectedIds.Contains(key))
                    return false;
            }
            return true;
        }

        public void InitPresets()
        {
            _PresetsPanel.Controls.Clear();
            _PresetConstrollers.Clear();
            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr("Presets:");
            nameLabel.TextAlign = ContentAlignment.MiddleLeft;
            _PresetsPanel.Controls.Add(nameLabel);

            foreach (string key in _Presets._Presets.Keys)
            {
                CheckBox checkBox = new CheckBox();
                checkBox.Text = TranslationHelper.Instance().Tr(key);
                _ToolTip1.SetToolTip(checkBox, TranslationHelper.Instance().Tr(_Presets._Presets[key].desc));

                PresetController controller = new PresetController();
                controller.name = key;
                controller.enabled = PresetState(key);
                checkBox.Checked = controller.enabled;
                controller.checkBox = checkBox;
                checkBox.CheckedChanged += controller.onClicked;
                controller.parent = this;
                _PresetConstrollers.Add(key, controller);
                _PresetsPanel.Controls.Add(checkBox);
            }

            Label saveLabel = new Label();
            saveLabel.Text = TranslationHelper.Instance().Tr("Save current:");
            saveLabel.TextAlign = ContentAlignment.MiddleLeft;
            _PresetsPanel.Controls.Add(saveLabel);

            _NameBox = new TextBox();
            _NameBox.Text = "custom";
            _NameBox.Width = _PresetsPanel.Width - 10;
            _NameBox.Anchor = (AnchorStyles.Right | AnchorStyles.Left);
            _PresetsPanel.Controls.Add(_NameBox);

            Button button = new Button();
            button.Text = TranslationHelper.Instance().Tr("Save");
            button.Width = _PresetsPanel.Width - 10;
            button.Anchor = (AnchorStyles.Right | AnchorStyles.Left);
            button.Click += onSaveClicked;
            _PresetsPanel.Controls.Add(button);
        }

        public void onChecked(object sender, EventArgs e)
        {
            foreach (string key in _Binding.Keys)
            {
                CheckBox checkBox = _Binding[key];
                if (checkBox.Checked && !_SelectedIds.Contains(key))
                    _SelectedIds.Add(key);
                if (!checkBox.Checked && _SelectedIds.Contains(key))
                    _SelectedIds.Remove(key);
            }
            UpdateExcludeList();
        }

        public void SetGroup(int index)
        {
            _Binding.Clear();
            _ItemsPanel.Controls.Clear();
            foreach(GItem item in _GameModel.GetAllT<GItem>())
            {
                if (item.item_cat != index)
                    continue;
                CheckBox checkBox = new CheckBox();
                checkBox.Width = 300;
                checkBox.Text = item.name_txt.value.text;
                checkBox.Checked = _SelectedIds.Contains(item.item_id);
                checkBox.CheckStateChanged += onChecked;
                _ToolTip1.SetToolTip(checkBox, item.item_id + " " + item.desc_txt.value.text);
                _Binding.Add(item.item_id, checkBox);
                _ItemsPanel.Controls.Add(checkBox);
            }
        }

        public void onGroupClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            int index = _GroupsBindings.IndexOf(button);
            if (index >= 0)
                SetGroup(index);
        }

        public void onSaveClicked(object sender, EventArgs e)
        {
            string name = _NameBox.Text;
            if (_Presets._Presets.ContainsKey(name))
            { 
                if (MessageBox.Show(TranslationHelper.Instance().Tr("Preset with same name already exist. Rewrite?"), "", MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    return;
                }
            }
            _Presets.SavePreset(name, _SelectedIds);
            InitPresets();
        }

        public void InitGroups()
        {
            _GroupsPanel.Controls.Clear();
            _GroupsBindings.Clear();
            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr("Types:");
            nameLabel.TextAlign = ContentAlignment.MiddleLeft;
            _GroupsPanel.Controls.Add(nameLabel);

            for(int i = 0; i < _GroupNames.Count; ++i)
            {
                Button button = new Button();
                button.Text = NameForType(i);
                button.Width = _GroupsPanel.Width - 10;
                button.Anchor = (AnchorStyles.Right | AnchorStyles.Left);
                button.Click += onGroupClicked;
                _GroupsPanel.Controls.Add(button);
                _GroupsBindings.Add(button);
            }
            if (_GroupNames.Count > 0)
                SetGroup(0);
        }

        public void Init(string type)
        {
            _GroupNames.Clear();
            List<LmagItm> itemCats = _GameModel.GetAllT<LmagItm>();
            foreach(LmagItm itemCat in itemCats)
            {
                string name = itemCat.text;
                _GroupNames.Add(name.Replace("L_", ""));
            }
        }

        public string NameForType(int type)
        {
            return TranslationHelper.Instance().Tr(_GroupNames[type]);
        }

        public List<string> Excluded()
        {
            return _SelectedIds;
        }


        public TableLayoutPanel FormWidget()
        {
            if (_Panel == null)
            {
                _Presets.LoadPresets();
                _Panel = new TableLayoutPanel();
                _Panel.RowCount = 0;
                _Panel.AutoSize = true;
                
                _Panel.Dock = DockStyle.Left;
                Padding pad = _Panel.Padding;
                pad.Bottom = 45;
                pad.Right = 45;
                _Panel.Padding = pad;
                int index = 0;

                _PresetsPanel = new FlowLayoutPanel();
                _PresetsPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
                _PresetsPanel.AutoSize = false;
                _PresetsPanel.Width = 140;
                _PresetsPanel.AutoScroll = true;
                foreach (string id in _Presets._Presets["Default"].ids)
                {
                    GItem item = _GameModel.GetObjectT<GItem>(id);
                    if (item == null)
                        continue;
                    _SelectedIds.Add(id);
                }

                _Panel.Controls.Add(_PresetsPanel, index++, 0);

                _GroupsPanel = new FlowLayoutPanel();
                _GroupsPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
                _GroupsPanel.AutoSize = false;
                _GroupsPanel.Width = 180;
                _GroupsPanel.AutoScroll = true;
                _GroupsPanel.BorderStyle = BorderStyle.FixedSingle;

                _Panel.Controls.Add(_GroupsPanel, index++, 0);

                _ItemsPanel = new FlowLayoutPanel();
                _ItemsPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
                _ItemsPanel.AutoSize = false;
                _ItemsPanel.Width = 340;
                _ItemsPanel.AutoScroll = true;
                _ItemsPanel.BorderStyle = BorderStyle.FixedSingle;

                _Panel.Controls.Add(_ItemsPanel, index++, 0);

                _ExcludedView = new ListView();
                _ExcludedView.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
                _ExcludedView.Width = 290;
                _ExcludedView.View = View.Details;
                _ExcludedView.ShowItemToolTips = true;
                _ExcludedView.Columns.Add(TranslationHelper.Instance().Tr("Excluded(double click to remove):"), 280);
                _ExcludedView.MouseDoubleClick += onExcludedItemClicked;
                _Panel.Controls.Add(_ExcludedView, index++, 0);
            }
            InitPresets();
            InitGroups();
            UpdateExcludeList();

            return _Panel;
        }

        public void onExcludedItemClicked(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = _ExcludedView.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                if (_Binding.ContainsKey(_SelectedIds[item.Index]))
                    _Binding[_SelectedIds[item.Index]].Checked = false;
                else
                    _SelectedIds.RemoveAt(item.Index);
                
                UpdateExcludeList();
            }
            else
                Log("WTF");
        }
    }
}
