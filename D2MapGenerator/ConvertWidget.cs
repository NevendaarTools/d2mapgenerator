﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using System.IO;
using NevendaarTools.DataTypes;
using NevendaarTools.Transform;
using System.Diagnostics;
using NevendaarTools.MapUtils;

namespace D2MapEdit
{
    public partial class ConvertWidget : UserControl
    {
        GameModel modelFrom = new GameModel();
        GameModel modelTo = new GameModel();
        MapModel mapModel = new MapModel();
        SettingsManager settings = SettingsManager.Instance("convert_settings.xml");
        FolderBrowserDialog browserDialog = new FolderBrowserDialog();
        MapEditor modificationHelper = new MapEditor();
        private MapReader mapReader = new MapReader();

        private void Log(string str)
        {
            logTextBox.AppendText(str + "\n");
        }

        public void ReloadTranslation()
        {
            openButton.Text = TranslationHelper.Instance().Tr("Open map");
            saveButton.Text = TranslationHelper.Instance().Tr("Save map");
            processFolderButton.Text = TranslationHelper.Instance().Tr("ProcessFolder");
            fromLabel.Text = TranslationHelper.Instance().Tr("Game path(from)");
            toLabel.Text = TranslationHelper.Instance().Tr("Game path(to)");
            resultFolderButton.Text = TranslationHelper.Instance().Tr("Open result folder");
            singleGroupBox.Text = TranslationHelper.Instance().Tr("Single map");
            folderGroupBox.Text = TranslationHelper.Instance().Tr("Folder");
        }

        public ConvertWidget()
        {
            InitializeComponent();
            ReloadTranslation();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;

            pathFromTextBox.Text = settings.Value("GamePathFrom", "");
            textBoxPathTo.Text = settings.Value("GamePathTo", "");
            ProcessFolderTextBox.Text = settings.Value("ConvertPath", "");
        }

        private void selectPathFromButton_Click(object sender, EventArgs e)
        {
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            pathFromTextBox.Text = browserDialog.SelectedPath;
        }

        private void buttonSelectPathTo_Click(object sender, EventArgs e)
        {
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            textBoxPathTo.Text = browserDialog.SelectedPath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(ProcessFolderTextBox.Text))
            {
                Log("Failed to find dir to process : " + ProcessFolderTextBox.Text);
                return;
            }
            if (!InitModels())
                return;
            settings.SetValue("ConvertPath", ProcessFolderTextBox.Text);
            //try
            {
                string dir = ProcessFolderTextBox.Text + "/converted";
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                IEnumerable<string> allfiles = Directory.EnumerateFiles(ProcessFolderTextBox.Text, "*.sg");
                foreach (string filename in allfiles)
                {
                    FileInfo info = new FileInfo(filename);
                    ProcessMap(filename, dir + "/" + info.Name);
                }
            }
            //catch (Exception)
            {
                Log("Failed to process dir " + ProcessFolderTextBox.Text);
            }
        }

        string FindSimilarUnit(string unit)
        {
            Gunit before = modelFrom.GetObjectT<Gunit>(unit.ToLower());
            List<KeyValuePair<string, int>> candidates = new List<KeyValuePair<string, int>>();
            List<Gunit> units = modelTo.GetAllT<Gunit>();
            foreach (Gunit after in units)
            {
                if (before.race_id.key == after.race_id.key &&
                    before.size_small == after.size_small &&
                    before.water_only == after.water_only &&
                    before.subrace.key == after.subrace.key &&
                    before.branch.key == after.branch.key &&
                    before.unit_cat.key == after.unit_cat.key)
                    candidates.Add(new KeyValuePair<string, int>(after.unit_id, CostHelper.G(before.enroll_c) - CostHelper.G(after.enroll_c)));
            }
            if (candidates.Count == 0)
            {
                foreach (Gunit after in units)
                {
                    if (before.size_small == after.size_small &&
                        before.water_only == after.water_only &&
                        before.branch.key == after.branch.key &&
                        before.unit_cat.key == after.unit_cat.key)
                        candidates.Add(new KeyValuePair<string, int>(after.unit_id, CostHelper.G(before.enroll_c) - CostHelper.G(after.enroll_c)));
                }
                if (candidates.Count == 0)
                {
                    foreach (Gunit after in units)
                    {
                        if (before.size_small == after.size_small &&
                            before.water_only == after.water_only &&
                            before.unit_cat.key == after.unit_cat.key)
                            candidates.Add(new KeyValuePair<string, int>(after.unit_id, CostHelper.G(before.enroll_c) - CostHelper.G(after.enroll_c)));
                    }
                }
                if (candidates.Count == 0)
                {
                    foreach (Gunit after in units)
                    {
                        if (before.size_small == after.size_small &&
                            before.water_only == after.water_only &&
                            before.subrace.key == after.subrace.key)
                            candidates.Add(new KeyValuePair<string, int>(after.unit_id, CostHelper.G(before.enroll_c) - CostHelper.G(after.enroll_c)));
                    }
                }
                if (candidates.Count == 0)
                {
                    Log("Failed to FindSimilarUnit : " + unit);
                    return unit.ToUpper();
                }
            }

            candidates = candidates.OrderBy(u => Math.Abs(u.Value)).ToList();
            return candidates.First().Key.ToUpper();
        }
        string FindSimilarItem(string item)
        {
            if (item == "g000000000")
                return item.ToUpper();
            GItem before = modelFrom.GetObjectT<GItem>(item.ToLower());
            List<KeyValuePair<string, int>> candidates = new List<KeyValuePair<string, int>>();
            List<GItem> items = modelTo.GetAllT<GItem>();
            foreach (GItem after in items)
            {
                if (before.item_cat == after.item_cat)
                    candidates.Add(new KeyValuePair<string, int>(after.item_id, CostHelper.G(before.value) - CostHelper.G(after.value)));
            }
            if (candidates.Count == 0)
            {
                Log("Failed to FindSimilarItem : " + item);
                return item.ToUpper();
            }

            candidates = candidates.OrderBy(u => Math.Abs(u.Value)).ToList();
            return candidates.First().Key.ToUpper();
        }
        string FindSimilarLmark(string lmark)
        {
            GLmark before = modelFrom.GetObjectT<GLmark>(lmark);
            List<GLmark> items = modelTo.GetAllT<GLmark>();
            foreach (GLmark after in items)
            {
                if (before.mountain == after.mountain &&
                    before.cx == after.cx &&
                    before.cy == after.cy &&
                    before.category == after.category)
                    return after.lmark_id.ToUpper();
            }
            foreach (GLmark after in items)
            {
                if (before.mountain == after.mountain &&
                    before.cx == after.cx &&
                    before.cy == after.cy)
                    return after.lmark_id.ToUpper();
            }
            Log("Failed to FindSimilarLmark : " + lmark + " " + before.name_txt.value.text);
            return lmark.ToUpper();
        }

        private void ProcessMap(string path, string outPath = null)
        {
            mapReader.ReadMap(mapModel, path);
            mapInfoWidget1.SetMap(ref mapModel, modelFrom);
            //process

            List<D2Unit> units = mapModel.GetListByType<D2Unit>();
            List<D2Item> items = mapModel.GetListByType<D2Item>();
            List<D2LandMark> lmarks = mapModel.GetListByType<D2LandMark>();
            List<D2Ruin> ruins = mapModel.GetListByType<D2Ruin>();
            List<D2StackTemplate> templates = mapModel.GetListByType<D2StackTemplate>();
            List<D2Merchant> merchants = mapModel.GetListByType<D2Merchant>();

            foreach (D2Unit unit in units)
            {
                bool changed = false;
                if (modelTo.GetObjectT<Gunit>(unit._Type.ToLower()) == null)
                {
                    unit._Type = FindSimilarUnit(unit._Type).ToUpper();
                    changed = true;
                }
                if (unit._Level < modelTo.GetObjectT<Gunit>(unit._Type.ToLower()).level)
                {
                    unit._Level = modelTo.GetObjectT<Gunit>(unit._Type.ToLower()).level;
                    changed = true;
                }
                for (int i = unit._Mods.Count - 1; i >= 0; --i)
                {
                    if (modelTo.GetObjectT<Gmodif>(unit._Mods[i].ToLower()) == null)
                        unit._Mods.RemoveAt(i);
                }
                if (changed)
                    mapModel.Replace(unit);
            }
            foreach (D2Item item in items)
            {
                bool changed = false;
                if (modelTo.GetObjectT<GItem>(item._type.ToLower()) == null)
                {
                    item._type = FindSimilarItem(item._type).ToUpper();
                    changed = true;
                }
                if (changed)
                    mapModel.Replace(item);
            }
            foreach (D2LandMark lmark in lmarks)
            {
                bool changed = false;
                GLmark lmarkTo = modelTo.GetObjectT<GLmark>(lmark._Type.ToLower());
                GLmark lmarkFrom = modelFrom.GetObjectT<GLmark>(lmark._Type.ToLower());
                if (lmarkTo == null ||
                    (lmarkTo.cx != lmarkFrom.cx))
                {
                    string newType = FindSimilarLmark(lmark._Type.ToLower()).ToUpper();
                    if (newType == lmark._Type.ToUpper())
                    {
                        Log("pos = " + lmark._posX.ToString() + " - " + lmark._posY.ToString());
                        //GameLMark before = modelFrom._LMarks[newType.ToLower()];

                        //D2Plan plan = mapModel.GetById<D2Plan>("PN0000"); 
                        //for(int i = plan._elements.Count - 1; i >= 0; --i)
                        //{
                        //    if (plan._elements[i]._id.ToUpper() == newType)
                        //    {
                        //        int x = plan._elements[i]._posX;
                        //        int y = plan._elements[i]._posY;
                        //        if ((lmark._posX >= x && x <= lmark._posX + before.sizeX) &&
                        //            (lmark._posY >= y && y <= lmark._posY + before.sizeY))
                        //        {
                        //            plan._elements.RemoveAt(i);
                        //        }
                        //    }
                        //}
                        //mapModel.Replace(plan);
                        //mapModel.RemoveDataBlockById(lmark.ObjId());
                    }
                    else
                    {
                        lmark._Type = newType;
                        changed = true;
                    }
                }
                if (changed)
                    mapModel.Replace(lmark);
            }
            foreach (D2Ruin ruin in ruins)
            {
                bool changed = false;
                if (modelTo.GetObjectT<GItem>(ruin._item.ToLower()) == null)
                {
                    ruin._item = FindSimilarItem(ruin._item.ToLower()).ToUpper();
                    changed = true;
                }
                if (changed)
                    mapModel.Replace(ruin);
            }

            foreach (D2StackTemplate tmplt in templates)
            {
                bool changed = false;
                for (int i = 0; i < 6; i++)
                {
                    if (tmplt._Unit[i] == "G000000000")
                        continue;

                    Gunit unitTo = modelTo.GetObjectT<Gunit>(tmplt._Unit[i].ToUpper());
                    if (unitTo == null)
                    {
                        if (tmplt._Leader == tmplt._Unit[i])
                        {
                            tmplt._Leader = FindSimilarUnit(tmplt._Leader.ToUpper());
                            tmplt._Unit[i] = tmplt._Leader;
                        }
                        else
                        {
                            tmplt._Unit[i] = FindSimilarUnit(tmplt._Unit[i].ToUpper());
                        }
                        changed = true;
                        unitTo = modelTo.GetObjectT<Gunit>(tmplt._Unit[i].ToUpper());
                    }
                    if (tmplt._UnitLvl[i] < unitTo.level)
                    {
                        tmplt._UnitLvl[i] = unitTo.level;
                        changed = true;
                    }
                }
                for (int i = tmplt._Mods.Count - 1; i >= 0; --i)
                {
                    if (modelTo.GetObjectT<Gmodif>(tmplt._Mods[i]._Modid.ToLower()) == null)
                    {
                        tmplt._Mods.RemoveAt(i);
                        changed = true;
                    }
                }

                if (changed)
                    mapModel.Replace(tmplt);
            }

            foreach (D2Merchant merchant in merchants)
            {
                bool changed = false;
                for (int i = 0; i < merchant._Items.Count; ++i)
                {
                    GItem itemTo = modelTo.GetObjectT<GItem>(merchant._Items[i]._ItemId.ToLower());
                    if (itemTo == null)
                    {
                        merchant._Items[i]._ItemId = FindSimilarItem(merchant._Items[i]._ItemId.ToLower()).ToUpper();
                        changed = true;
                    }

                }
                if (changed)
                    mapModel.Replace(merchant);
            }
            List<D2Mercs> merks = mapModel.GetListByType<D2Mercs>();
            foreach (D2Mercs merk in merks)
            {
                bool changed = false;
                for (int i = 0; i < merk._Units.Count; ++i)
                {
                    Gunit unitFrom = modelFrom.GetObjectT<Gunit>(merk._Units[i]._UnitId.ToLower());
                    Gunit unitTo = modelTo.GetObjectT<Gunit>(merk._Units[i]._UnitId.ToLower());
                    Log("before = " + merk._Units[i]._UnitId + unitFrom.name_txt.value.text);
                    if (unitTo == null)
                    {
                        merk._Units[i]._UnitId = FindSimilarUnit(merk._Units[i]._UnitId.ToLower()).ToUpper();
                        changed = true;
                        unitTo = modelTo.GetObjectT<Gunit>(merk._Units[i]._UnitId.ToLower());
                    }
                    if (merk._Units[i]._UnitLevel < unitTo.level)
                    {
                        merk._Units[i]._UnitLevel = unitTo.level;
                        changed = true;
                    }
                    Log("after = " + merk._Units[i]._UnitId + unitTo.name_txt.value.text);

                }
                if (changed)
                    mapModel.Replace(merk);
            }

            List<D2Mage> mages = mapModel.GetListByType<D2Mage>();
            foreach (D2Mage mage in mages)
            {
                bool changed = false;
                for (int i = mage._Spells.Count - 1; i >= 0; --i)
                {
                    if (modelTo.GetObjectT<Gspell>(mage._Spells[i].ToLower()) == null)
                    {
                        mage._Spells.RemoveAt(i);
                        changed = true;
                    }

                }
                if (changed)
                    mapModel.Replace(mage);
            }

            //

            if (outPath != null)
            {
                MapEditor.Rename(ref mapModel, mapInfoWidget1.MapName(), mapInfoWidget1.MapAuthor());
                if (MapReader.SaveMap(mapModel, outPath))
                {
                    Log("MapSaved: " + outPath);
                }
                else
                {
                    Log("Failed to save map : " + outPath);
                }
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (!InitModels())
                return;

            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файлы карт|*.sg;";
            openDialog.InitialDirectory = settings.Value("openPath", ".");
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;

            settings.SetValue("openPath", System.IO.Path.GetDirectoryName(openDialog.FileName));

            ProcessMap(openDialog.FileName);
        }

        private bool InitModels()
        {
            logTextBox.Text = "";
            if (!System.IO.File.Exists(pathFromTextBox.Text + "\\globals\\Tglobal.dbf"))
            {
                Log("Invalid path! File " + pathFromTextBox.Text + "\\globals\\Tglobal.dbf not found!!!");
                return false;
            }
            modelFrom.Load(pathFromTextBox.Text, false);
            settings.SetValue("GamePathFrom", pathFromTextBox.Text);

            if (!System.IO.File.Exists(textBoxPathTo.Text + "\\globals\\Tglobal.dbf"))
            {
                Log("Invalid path! File " + textBoxPathTo.Text + "\\globals\\Tglobal.dbf not found!!!");
                return false;
            }
            modelTo.Load(textBoxPathTo.Text, false);
            settings.SetValue("GamePathTo", textBoxPathTo.Text);
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Файлы карт|*.sg;";
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            MapReader.SaveMap(mapModel, saveFileDialog1.FileName);
        }

        private void SelectProcessFolderButton_Click(object sender, EventArgs e)
        {
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            ProcessFolderTextBox.Text = browserDialog.SelectedPath;
        }

        private void resultFolderButton_Click(object sender, EventArgs e)
        {
            string path = ProcessFolderTextBox.Text + "\\converted";
            Process.Start(new ProcessStartInfo("explorer.exe", path));
        }
    }
}
