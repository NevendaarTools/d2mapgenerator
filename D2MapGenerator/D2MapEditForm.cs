﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using D2MapEdit.Transform;
using NevendaarTools;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;
using NevendaarTools.Transform;
using static NevendaarTools.Logger;

namespace D2MapEdit
{
    public partial class D2MapEditForm : Form
    {
        StackGenAdapter _Adapter = new StackGenAdapter();
        MapTransformExecutor _TransformExecutor = new MapTransformExecutor();
        SettingsManager _Settings = SettingsManager.Instance("settings.xml");
        //ControlBinder _Binder = new ControlBinder();
        ExcludeSelectWidget _ExcludeItemsWidget;
        List<string> _Excluded = new List<string>();
        OptionHelper optionHelper = new OptionHelper();
        private string _MapToRegen = "";


        public GameModel _GameModel = new GameModel();
        private void Log(LogLevel level, string str)
        {
            logTextBox.AppendText("[" + level + "]" + str + "\n");
        }

        public D2MapEditForm()
        {
            InitializeComponent();
            Logger.Instance()._LogDelegate += Log; 
            pathTextBox.Text = _Settings.Value("GamePath");
            //_TransformExecutor.SetRegenerator(new DummyRegenerator());
            _TransformExecutor.SetRegenerator(_Adapter);

            _TransformExecutor._Transforms.Add(new MapStackTransform());
            _TransformExecutor._Transforms.Add(new RuinTransform());
            _TransformExecutor._Transforms.Add(new MapBagTransform());
            _TransformExecutor._Transforms.Add(new MerchantTransform());
            _TransformExecutor._Transforms.Add(new VillageIncomeTransform());
            _ExcludeItemsWidget = new ExcludeSelectWidget(_GameModel);
            _Excluded = PresetManager.GetPresetIdsFromFile("Resources/ExcludeIDs.txt", null);
            _Adapter._ExcludedIds.AddRange(_Excluded);

            //ConfigureWidget confWgt = new ConfigureWidget();
            //tabControl1.TabPages[2].Controls.Add(confWgt);
            //tabControl1.Controls.Add(confWgt);
            InitTranslationList();
            parseGameButton_Click(null, null);
            tabControl1.TabPages.RemoveAt(1);
            //tabControl1.TabPages.RemoveAt(1);
            //tabControl1.TabPages.RemoveAt(1);

            _TransformExecutor.RestoreSettings("Regenerator.json");
            optionHelper.Reload();
        }

        void InitTranslationList()
        {
            langComboBox.Items.Add("en");
            string[] s;
            s = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*.tr");
            foreach (string s2 in s)
            {
                langComboBox.Items.Add(System.IO.Path.GetFileNameWithoutExtension(s2));
            }
            string lang = _Settings.Value("lang", "ru");
            if (!_Settings.HasValue("lang"))
                _Settings.SetValue("lang", lang);
            langComboBox.SelectedIndex = langComboBox.Items.IndexOf(lang);
            LoadTransalation();
        }

        public void LoadTransalation()
        {
            string lang = langComboBox.Text;
            _Settings.SetValue("lang", lang);

            TranslationHelper.Instance().LoadTranslation(lang);
            TranslationHelper.Instance().SetTranslation(lang);
            textBox1.Text = TranslationHelper.Instance().Tr("RegenWarning");
            label2.Text = TranslationHelper.Instance().Tr("Game path:");
            openButton.Text = TranslationHelper.Instance().Tr("Open map");
            button1.Text = TranslationHelper.Instance().Tr("Exclude items");
            processButton.Text = TranslationHelper.Instance().Tr("Regenerate map");
            saveButton.Text = TranslationHelper.Instance().Tr("Save result map as..");
            groupBox2.Text = TranslationHelper.Instance().Tr("Configure generator");
            tabControl1.TabPages[0].Text = TranslationHelper.Instance().Tr("MapRegeneration");
            parseGameButton.Text = TranslationHelper.Instance().Tr("Parse");
            if (tabControl1.TabPages.Count > 1)
            {
                tabControl1.TabPages[1].Text = TranslationHelper.Instance().Tr("MapGeneration(alpha)");

                tabControl1.TabPages[2].Text = TranslationHelper.Instance().Tr("MapConfiguration");
                tabControl1.TabPages[3].Text = TranslationHelper.Instance().Tr("Convert");
            }
            optionHelper.Reload();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            mapInfoWidget1.Apply();
            _TransformExecutor.SaveMap(saveFileDialog1.FileName);
        }

        private void processButton_Click(object sender, EventArgs e)
        {
            logTextBox.Text = "";
            if (_MapToRegen == "")
                return;
            _TransformExecutor.LoadMap(_MapToRegen);
            {
                List<D2Stack> stacks = _TransformExecutor._MapModel.GetListByType<D2Stack>();

                for (int i = 0; i < stacks.Count; ++i)
                {
                    bool deadStack = true;
                    foreach (string unitId in stacks[i]._Unit)
                    {
                        if (unitId == "000000")
                            continue;
                        D2Unit unit = _TransformExecutor._MapModel.GetById<D2Unit>(unitId);
                        if (unit == null)
                        {
                            Logger.Log("unit not found by id " + unitId);
                            Logger.Log("stack id " + stacks[i].ObjId() + "pos:" + stacks[i]._posX.ToString() + " " + stacks[i]._posY.ToString());
                            break;
                        }
                        if (unit._HP > 0)
                        {
                            deadStack = false;
                            break;
                        }
                    }
                    if (deadStack) {
                        Logger.Log("Dead stack found!");
                        Logger.Log("stack id " + stacks[i].ObjId() + "pos:" + stacks[i]._posX.ToString() + " " + stacks[i]._posY.ToString());
                    }
                }
            }
            List<D2Capital> capitals = _TransformExecutor._MapModel.GetListByType<D2Capital>();
            List<string> playerIds = new List<string>();
            _Adapter.CapPos = new RandomStackGenerator.Point[capitals.Count];
            for (int i = 0; i < capitals.Count; i++)
            {
                _Adapter.CapPos[i] = new RandomStackGenerator.Point(capitals[i]._PosX, capitals[i]._PosY);
                playerIds.Add(capitals[i]._Owner);
            }
            _Adapter.MapLords = new string[playerIds.Count];
            List<D2Player> players = _TransformExecutor._MapModel.GetListByType<D2Player>();
            int index = 0;
            for (int i = 0; i < players.Count; i++)
            {
                if (playerIds.Contains(players[i]._objId))
                {
                    Logger.Log(players[i]._lordId);
                    _Adapter.MapLords[index++] = players[i]._lordId;
                }
            }
            _Adapter.MinesAmount = new RandomStackGenerator.AllDataStructues.Cost();
            List<D2Crystal> crystals = _TransformExecutor._MapModel.GetListByType<D2Crystal>();
            foreach(D2Crystal crystal in crystals)
            {
                switch(crystal._Resource)
                {
                    case (int)Enums.ResourceType.GOLG:
                        _Adapter.MinesAmount.Gold++;
                        break;
                    case (int)Enums.ResourceType.EMPIRE:
                        _Adapter.MinesAmount.Blue++;
                        break;
                    case (int)Enums.ResourceType.DEMONS:
                        _Adapter.MinesAmount.Red++;
                        break;
                    case (int)Enums.ResourceType.CLANS:
                        _Adapter.MinesAmount.White++;
                        break;
                    case (int)Enums.ResourceType.ELVES:
                        _Adapter.MinesAmount.Green++;
                        break;
                    case (int)Enums.ResourceType.UNDEAD:
                        _Adapter.MinesAmount.Black++;
                        break;
                    default:
                        Logger.Log("Unknown resource type : " + crystal._Resource.ToString());
                        break;
                }
            }
            processButton.Enabled = true;

            //_TransformExecutor.SetRegenerator(_Adapter);
            //_TransformExecutor.SetRegenerator(new DummyRegenerator());

            //_Binder.Pull(ref _TransformExecutor._Options);
            _TransformExecutor.Execute();
            saveButton.Enabled = true;
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            _MapToRegen = openFileDialog1.FileName;
            processButton_Click(sender, e);
            mapInfoWidget1.SetMap(ref _TransformExecutor._MapModel, _TransformExecutor._GameModel);
            if (!mapInfoWidget1.MapName().EndsWith("_gen"))
                mapInfoWidget1.SetMapName(mapInfoWidget1.MapName() + "_gen");
        }

        private void selectPathButton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = pathTextBox.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            pathTextBox.Text = folderBrowserDialog1.SelectedPath;
            parseGameButton_Click(null, null);
        }

        private void parseGameButton_Click(object sender, EventArgs e)
        {
            logTextBox.Text = "";
            if (!System.IO.File.Exists(pathTextBox.Text + "\\globals\\Tglobal.dbf"))
            {
                Logger.Log("Invalid path! File " + pathTextBox.Text + "\\globals\\Tglobal.dbf not found!!!");
                groupBox1.Enabled = false;
                return;
            }
            _GameModel.Load(pathTextBox.Text, true);
            _Adapter.LoadGameData(_GameModel);
            _Settings.SetValue("GamePath", pathTextBox.Text);
            configureWidget1.model = _GameModel;

            _TransformExecutor.LoadGameData(pathTextBox.Text, ref _Settings);
            groupBox1.Enabled = true;

            optionHelper.Init(_TransformExecutor, flowLayoutPanel1);
            optionHelper.Reload();
        }

        private void D2MapEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _TransformExecutor.SaveSettings("Regenerator.json");
        }

        private void optionInfoButton_Click(object sender, EventArgs e)
        {
            //string text = String.Join("\n", _TransformExecutor._Options.ToDescriptionsList().ToArray());
            //MessageBox.Show(text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form wrapper = new Form();
            _ExcludeItemsWidget.Init("");
            TableLayoutPanel panel = _ExcludeItemsWidget.FormWidget();
            wrapper.Height = (int)(this.Height * 0.7f);
            wrapper.Width = 1000;// (int)(this.Width * 0.7f);
            Padding pad = wrapper.Padding;
            pad.Bottom = 400;
            wrapper.Padding = pad;

            panel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left);
            panel.Height = wrapper.Height;
            panel.Width = wrapper.Width;
            panel.AutoSize = true;
            panel.AutoScroll = true;
            wrapper.Controls.Add(panel);

            wrapper.Text = TranslationHelper.Instance().Tr("Exclude items:");
            wrapper.ShowDialog();
            _Adapter._ExcludedIds.Clear();
            foreach (string id in _Excluded)
            {
                GItem item = _GameModel.GetObjectT<GItem>(id);
                if (item != null)
                    continue;
                _Adapter._ExcludedIds.Add(id);
            }
            _Adapter._ExcludedIds.AddRange(_ExcludeItemsWidget.Excluded());
            Logger.Log("Excluded items: ");
            foreach (string id in _Adapter._ExcludedIds)
            {
                GItem item = _GameModel.GetObjectT<GItem>(id);
                if (item != null)
                Logger.Log(id + " - " + item.name_txt.value.text);
                else
                    Logger.Log(id);
            }
        }

        private void langComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTransalation();
        }
    }
}
