﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace NevendaarTools.Transform
{
    //public class ControlBinder
    //{
    //    public Dictionary<string, Control> _binding = new Dictionary<string, Control>();
    //    ToolTip toolTip1;

    //    public ControlBinder()
    //    {
    //        toolTip1 = new ToolTip();
    //        toolTip1.AutoPopDelay = 5000;
    //        toolTip1.InitialDelay = 400;
    //        toolTip1.ReshowDelay = 500;
    //    }

    //    public void Pull(ref OptionHolder holder)
    //    {
    //        for (int i = 0; i < holder._Options.Count; ++i)
    //        {
    //            OptionHolder.OptionsGroup group = holder._Options[i];

    //            for (int k = 0; k < group.options.Count; ++k)
    //            {
    //                Option option = group.options[k];
    //                string name = group.name + "_" + option.name;
    //                if (_binding.ContainsKey(name))
    //                {
    //                    if (option.type == Option.Type.Bool)
    //                    {
    //                        CheckBox checkBox = _binding[name] as CheckBox;
    //                        holder._Options[i].options[k].value = (checkBox.Checked) ? "True" : "False";
    //                    }
    //                    else if (option.type == Option.Type.Float || option.type == Option.Type.Int)
    //                    {
    //                        NumericUpDown numUpD = _binding[name] as NumericUpDown;
    //                        holder._Options[i].options[k].value = numUpD.Value.ToString();
    //                    }
    //                    else if (option.type == Option.Type.String)
    //                    {
    //                        TextBox textBox = _binding[name] as TextBox;
    //                        holder._Options[i].options[k].value = textBox.Text;
    //                    }
    //                    else if (option.type == Option.Type.Enum)
    //                    {
    //                        ComboBox textBox = _binding[name] as ComboBox;
    //                        holder._Options[i].options[k].value = textBox.Text;
    //                    }
    //                }
    //            }
    //        }
    //    }

        

    //    public class OptionsPanel: FlowLayoutPanel
    //    {
    //        private FlowLayoutPanel _optionPanel;
    //        private CheckBox _enableCheckBox;
    //        private CheckBox _debugCheckBox;
    //        private Label _nameLabel;

    //        public OptionsPanel()
    //        {
               
    //        }

    //        private void SetEnabledCheckbox(ref CheckBox checkbox)
    //        {
    //            _enableCheckBox = checkbox;
    //            _enableCheckBox.CheckStateChanged +=  UpdateEnabled;
    //        }

    //        public  void UpdateEnabled(object sender, EventArgs e)
    //        {
    //            if (_enableCheckBox == null)
    //                return;
    //            bool enabled = _enableCheckBox.Checked;
    //            _optionPanel.Visible = enabled;
    //            _nameLabel.Enabled = enabled;
    //            if (_debugCheckBox != null)
    //                _debugCheckBox.Enabled = enabled;
    //        }

    //        private FlowLayoutPanel FormHeader(ref ToolTip toolTip1, string name, string desc)
    //        {
    //            FlowLayoutPanel headerPanel = new FlowLayoutPanel();
    //            headerPanel.FlowDirection = FlowDirection.LeftToRight;
    //            headerPanel.Width = 350;
    //            headerPanel.Height = 21;
    //            headerPanel.Margin = new Padding(0);

    //            _nameLabel = new Label();
    //            _nameLabel.Text = name;
    //            _nameLabel.TextAlign = ContentAlignment.MiddleLeft;
    //            _nameLabel.Font = new Font(_nameLabel.Font, FontStyle.Bold);
    //            _nameLabel.TextAlign = ContentAlignment.BottomLeft;
    //            _nameLabel.Width = 130;
    //            _nameLabel.Height = 20;
    //            toolTip1.SetToolTip(_nameLabel, desc);
    //            headerPanel.Controls.Add(_nameLabel);

    //            return headerPanel;
    //        }

    //        public static OptionsPanel Construct(ref ToolTip toolTip1, ref Dictionary<string, Control> _binding, OptionHolder.OptionsGroup group)
    //        {
    //            OptionsPanel result = new OptionsPanel();
    //            result.FlowDirection = FlowDirection.TopDown;
    //            result.AutoSize = true;
    //            result.BorderStyle = BorderStyle.FixedSingle;

    //            FlowLayoutPanel groupBox = new FlowLayoutPanel();
    //            groupBox.FlowDirection = FlowDirection.TopDown;
    //            groupBox.AutoSize = true;
    //            result._optionPanel = groupBox;
    //            //groupBox.BorderStyle = BorderStyle.FixedSingle;

    //            FlowLayoutPanel headerPanel = result.FormHeader(ref toolTip1, TranslationHelper.Instance().Tr(group.name), 
    //                TranslationHelper.Instance().Tr("These options are applyed to ") + 
    //                TranslationHelper.Instance().Tr(group.target));

    //            groupBox.Controls.Add(headerPanel);

                
    //            foreach (Option option in group.options)
    //            {
    //                if (option.type == Option.Type.Bool)
    //                {
    //                    CheckBox checkBox = new CheckBox();
    //                    checkBox.Text = TranslationHelper.Instance().Tr(option.name);
    //                    checkBox.AutoSize = true;
    //                    //checkBox.Width = 220;
    //                    toolTip1.SetToolTip(checkBox, TranslationHelper.Instance().Tr(option.desc));
    //                    checkBox.Checked = option.value.Trim() == "True";
    //                    if (option.name == "Enabled")
    //                    {
    //                        result.SetEnabledCheckbox(ref checkBox);
    //                        headerPanel.Controls.Add(checkBox);
    //                    }
    //                    else if (option.name == "Debug")
    //                    {
    //                        result._debugCheckBox = checkBox;
    //                        headerPanel.Controls.Add(checkBox); 
    //                    }
    //                    else
    //                        groupBox.Controls.Add(checkBox);
    //                    _binding.Add(group.name + "_" + option.name, checkBox);
    //                }
    //                else if (option.type == Option.Type.Float || option.type == Option.Type.Int)
    //                {
    //                    FlowLayoutPanel panel = new FlowLayoutPanel();
    //                    panel.Margin = new Padding(0);
    //                    panel.FlowDirection = FlowDirection.LeftToRight;
    //                    panel.AutoSize = true;
    //                    NumericUpDown numUpD = new NumericUpDown();
    //                    if (option.type == Option.Type.Float)
    //                    {
    //                        numUpD.DecimalPlaces = 2;
    //                        numUpD.Increment = 0.05M;
    //                        numUpD.Minimum = Decimal.Parse(option.min);
    //                        numUpD.Maximum = Decimal.Parse(option.max);
    //                    }
    //                    else
    //                    {
    //                        numUpD.DecimalPlaces = 0;
    //                        numUpD.Increment = 1M;
    //                        numUpD.Minimum = Decimal.Parse(option.min);
    //                        numUpD.Maximum = Decimal.Parse(option.max);
    //                    }

    //                    string value = option.value;
    //                    value = value.Replace(",", ".");
    //                    value = value.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

    //                    numUpD.Value = decimal.Parse(value);
    //                    Label nameLabel = new Label();
    //                    nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
    //                    nameLabel.TextAlign = ContentAlignment.BottomLeft;
    //                    //nameLabel.AutoSize = true;
    //                    nameLabel.Width = 200;
    //                    nameLabel.Height = numUpD.Height;
    //                    toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
    //                    panel.Controls.Add(nameLabel);
    //                    panel.Controls.Add(numUpD);
    //                    groupBox.Controls.Add(panel);
    //                    _binding.Add(group.name + "_" + option.name, numUpD);
    //                }
    //                else if (option.type == Option.Type.String)
    //                {
    //                    FlowLayoutPanel panel = new FlowLayoutPanel();
    //                    panel.Margin = new Padding(0);
    //                    panel.FlowDirection = FlowDirection.LeftToRight;
    //                    panel.AutoSize = true;

    //                    TextBox textBox = new TextBox();
    //                    textBox.Multiline = false;
    //                    textBox.Text = option.value;
    //                    Label nameLabel = new Label();
    //                    nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
    //                    nameLabel.TextAlign = ContentAlignment.BottomLeft;
    //                    //nameLabel.AutoSize = true;
    //                    nameLabel.Width = 200;
    //                    nameLabel.Height = textBox.Height;
    //                    toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
    //                    panel.Controls.Add(nameLabel);
    //                    panel.Controls.Add(textBox);
    //                    groupBox.Controls.Add(panel);
    //                    _binding.Add(group.name + "_" + option.name, textBox);
    //                }
    //                else if (option.type == Option.Type.Enum)
    //                {
    //                    FlowLayoutPanel panel = new FlowLayoutPanel();
    //                    panel.Margin = new Padding(0);
    //                    panel.FlowDirection = FlowDirection.LeftToRight;
    //                    panel.AutoSize = true;

    //                    ComboBox comboBox = new ComboBox();
    //                    comboBox.DropDownStyle = ComboBoxStyle.DropDownList;

    //                    foreach (string variant in option.variants)
    //                    {
    //                        comboBox.Items.Add(variant);
    //                    }
    //                    comboBox.SelectedIndex = comboBox.Items.IndexOf(option.value);

    //                    Label nameLabel = new Label();
    //                    nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
    //                    nameLabel.TextAlign = ContentAlignment.BottomLeft;
    //                    //nameLabel.AutoSize = true;
    //                    nameLabel.Width = 200;
    //                    nameLabel.Height = comboBox.Height;
    //                    toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
    //                    panel.Controls.Add(nameLabel);
    //                    panel.Controls.Add(comboBox);
    //                    groupBox.Controls.Add(panel);
    //                    _binding.Add(group.name + "_" + option.name, comboBox);
    //                }
    //            }

    //            result.Controls.Add(headerPanel);
    //            result.Controls.Add(groupBox);
    //            result.UpdateEnabled(null, null);
    //            return result;
    //        }
    //    }

    //    public void Fill(ref OptionHolder holder, ref FlowLayoutPanel flowLayoutPanel1)
    //    {
    //        _binding.Clear();
    //        flowLayoutPanel1.Controls.Clear();

    //        foreach (OptionHolder.OptionsGroup group in holder._Options)
    //        {
    //            flowLayoutPanel1.Controls.Add(OptionsPanel.Construct(ref toolTip1, ref _binding, group));
    //        }
    //    }
    //}
}
