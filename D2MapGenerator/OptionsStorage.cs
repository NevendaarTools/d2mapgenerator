﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using NevendaarTools.Transform;

namespace D2MapEdit
{
    public interface OptionsStorage
    {
        int OptionsCount();
        int SetOption(int index, string value, ref bool neadReload);
        Option GetOptionAt(int index);
    }

    public class OptionHelper
    {
        private OptionsStorage storage;
        private List<OptionGroup> groups;
        private ToolTip toolTip1;
        private FlowLayoutPanel panel;
        Dictionary<string, Control> controls = new Dictionary<string, Control>();

        public void Init(OptionsStorage options, FlowLayoutPanel layoutPanel)
        {
            storage = options;
            panel = layoutPanel;
            //panel.FlowDirection = FlowDirection.TopDown;
            //panel.AutoScroll = true;
            //panel.WrapContents = true;
        }

        public OptionHelper()
        {
            toolTip1 = new ToolTip();
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 400;
            toolTip1.ReshowDelay = 500;
        }

        public class OptionGroup
        {
            public string groupid;
            public List<Option> options = new List<Option>();
            public List<int> optionIndexes = new List<int>();
        }

        public void Update()
        {
            int count = storage.OptionsCount();
            for (int i = 0; i < count; i++)
            {
                Option opt = storage.GetOptionAt(i);
                if (opt.status == Option.Status.Hiden)
                    continue;
                string name = opt.group + "-" + opt.name;
                if (controls.ContainsKey(name))
                    controls[name].Enabled = opt.status != Option.Status.Disabled;
            }
        }

        public void Reload()
        {
            controls.Clear();
            if (panel == null || storage == null)
                return;
            panel.Controls.Clear();
            groups = new List<OptionGroup>();
            Dictionary<string, int> binding = new Dictionary<string, int>();
            int count = storage.OptionsCount();
            for (int i = 0; i < count; i++)
            {
                Option opt = storage.GetOptionAt(i);
                bool tmp = false;
                storage.SetOption(i, opt.value, ref tmp);//to update disabled status for dependent properties
                if (opt.status == Option.Status.Hiden)
                    continue;
                if (binding.ContainsKey(opt.group))
                {
                    groups[binding[opt.group]].options.Add(opt);
                    groups[binding[opt.group]].optionIndexes.Add(i);
                }
                else
                {
                    OptionGroup group = new OptionGroup();
                    group.groupid = opt.group;
                    group.options.Add(opt);
                    group.optionIndexes.Add(i);
                    binding.Add(opt.group, groups.Count);
                    groups.Add(group);
                }
            }
            for(int i = 0; i < groups.Count; ++i)
            {
                panel.Controls.Add(FormGroupPanel(ref toolTip1, groups[i]));
            }
        }

        private Panel FormGroupPanel(ref ToolTip toolTip1, OptionGroup group)
        {
            Panel resultPanel = new Panel();
            //resultPanel.FlowDirection = FlowDirection.TopDown;
            resultPanel.Width = 340;
            resultPanel.Height = 25;
            resultPanel.BorderStyle = BorderStyle.FixedSingle;
            resultPanel.Margin = new Padding(3);

            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(group.groupid);
            nameLabel.TextAlign = ContentAlignment.MiddleLeft;
            nameLabel.Font = new Font(nameLabel.Font, FontStyle.Bold);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;
            nameLabel.Width = resultPanel.Width;
            nameLabel.Height = 20;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(group.groupid + "_desc"));
            resultPanel.Controls.Add(nameLabel);
            int i = 0;
            bool enabled = true;
            if (group.options[i].name == "Enabled")
            {
                Control control = OptionControl(ref toolTip1, ref storage, resultPanel.Width, group.optionIndexes[i]);
                if (control != null)
                {
                    resultPanel.Controls.Add(control);
                    control.Top = nameLabel.Top + 5;
                    nameLabel.Width = resultPanel.Width - 100;
                    control.Left = nameLabel.Width;
                    if (!Option.GetBoolOption(group.options[i]))
                    {
                        enabled = false;
                        resultPanel.Height += 3;
                    }
                    i += 1;
                }
            }
            if (group.options[i].name == "Debug")
            {
                Control control = OptionControl(ref toolTip1, ref storage, resultPanel.Width, group.optionIndexes[i]);
                if (control != null)
                {
                    resultPanel.Controls.Add(control);
                    control.Top = nameLabel.Top + 5;
                    nameLabel.Width = resultPanel.Width - 180;
                    control.Left = nameLabel.Width;
                    control.Enabled = enabled;
                    i += 1;
                }
            }
            nameLabel.Enabled = enabled;
            if (enabled)
            {
                for (; i < group.options.Count; ++i)
                {
                    Control control = OptionControl(ref toolTip1, ref storage, resultPanel.Width, group.optionIndexes[i]);
                    if (control == null)
                        continue;
                    controls.Add(group.groupid + "-" + group.options[i].name, control);
                    if (storage.GetOptionAt(group.optionIndexes[i]).status == Option.Status.Disabled)
                        control.Enabled = false;
                    resultPanel.Controls.Add(control);
                    control.Top = resultPanel.Height;
                    control.Left = 5;
                    resultPanel.Height += control.Height + 3;
                }
            }
            resultPanel.AutoSize = false;

            return resultPanel;
        }


        private Control OptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            Option option = optStorage.GetOptionAt(optIndex);
            if (option.status == Option.Status.Hiden)
                return null;
            if (option.type == Option.Type.Bool)
            {
                BoolOptionControl checkBox = new BoolOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                checkBox.requestReload += Reload;
                checkBox.requestUpdate += Update;
                return checkBox;
            }
            else if (option.type == Option.Type.Float || option.type == Option.Type.Int)
            {
                NumericOptionControl numControl = new NumericOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                numControl.requestReload += Reload;
                numControl.requestUpdate += Update;
                return numControl;
            }
            else if (option.type == Option.Type.String)
            {
                StringOptionControl strControl = new StringOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                strControl.requestReload += Reload;
                strControl.requestUpdate += Update;
                return strControl;
            }
            else if (option.type == Option.Type.Enum)
            {
                EnumOptionControl enumControl = new EnumOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                enumControl.requestReload += Reload;
                enumControl.requestUpdate += Update;
                return enumControl;
            }
            else if (option.type == Option.Type.SemgaList)
            {
                SemgaListOptionControl enumControl = new SemgaListOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                enumControl.requestReload += Reload;
                enumControl.requestUpdate += Update;
                return enumControl;
            }
            else if (option.type == Option.Type.PathSelect)
            {
                PathOptionControl enumControl = new PathOptionControl(ref toolTip1, ref optStorage, w, optIndex);
                enumControl.requestReload += Reload;
                enumControl.requestUpdate += Update;
                return enumControl;
            }
            Label label = new Label();
            label.Text = TranslationHelper.Instance().Tr(option.name); 
            label.TextAlign = ContentAlignment.MiddleLeft;
            label.Font = new Font(label.Font, FontStyle.Bold);
            label.TextAlign = ContentAlignment.BottomLeft;
            label.Width = w;
            label.Height = 20;
            toolTip1.SetToolTip(label, TranslationHelper.Instance().Tr(option.desc));
            return label;
        }
    }
}

