﻿using D2MapEdit;
using NevendaarTools.MapUtils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace NevendaarTools.Transform
{
    public class BaseTransform : OptionsStorage
    {
        protected List<Option> options = new List<Option>();
        protected string _Name;
        protected string _Target;

        public void AddOption(string name, bool value, string desc)
        {
            options.Add(Option.BoolOption(name, value, desc));
            options[options.Count - 1].group = _Name;
        }
        public void AddOption(string name, bool value)
        {
            AddOption(name, value, name + "_desc");
        }

        public void AddOption(string name, float value, string desc)
        {
            options.Add(Option.FloatOption(name, value, desc));
            options[options.Count - 1].group = _Name;
        }
        public void AddOption(string name, float value)
        {
            AddOption(name, value, name + "_desc");
        }

        public void AddOption(string name, int value, string desc)
        {
            options.Add(Option.IntOption(name, value, desc));
            options[options.Count - 1].group = _Name;
        }

        public void AddOption(string name, int value)
        {
            AddOption(name, value, name + "_desc");
        }

        public string Name() { return _Name; }
        public string Target() { return _Target; }

        public int OptionsCount()
        {
            return options.Count;
        }

        virtual public int SetOption(int index, string value, ref bool neadReload)
        {
            options[index].value = value;
            if (options[index].name == "Enabled")
            {
                neadReload = true;
            }
            return 0;
        }

        public bool Enabled()
        {
            return Option.GetBoolOption(options, "Enabled");
        }

        protected void Init(string name, string target)
        {
            _Name = name;
            _Target = target;
            options.Clear();
            AddOption("Enabled", true, "Enabled");
            AddOption("Debug", false, "Debug");
        }

        public Option GetOptionAt(int index)
        {
            return options[index];
        }

        public virtual void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {

        }
    }
}
