﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;

namespace NevendaarTools.Transform
{
    public class MapStackTransform : BaseTransform
    {
        //config
        public bool inVillages = true;
        public bool regenNeutral = true;
        public bool regenPlayable = false;
        public float playablePowerMult = 1.0f;
        public bool regenItems = true;

        //local data
        List<string> capitalGuards = new List<string>();
        List<string> villageGuards = new List<string>();
        List<string> objectsToRemove = new List<string>();
        List<D2Stack> stacksToReplace = new List<D2Stack>();
        Dictionary<string, StackTemplate> templates = new Dictionary<string, StackTemplate>();
        public MapStackTransform()
        {
            Init("Stack", "Stacks and items");

            AddOption("NeutralStacks", true);
            AddOption("PlayableRacesStacks", false);
            AddOption("PlayableRaceStacksExtraMultiplier", 1.0f);

            AddOption("StacksItems", true);
            AddOption("VillageVisiters", true);
            AddOption("VillageGarrison", true);
            AddOption("VillageItems", true);
            AddOption("StackTemplates", true);
        }

        public override int SetOption(int index, string value, ref bool neadReload)
        {
            base.SetOption(index, value, ref neadReload);
            if (options[index].name == "PlayableRacesStacks")
            {
                Option.GetOption(options, "PlayableRaceStacksExtraMultiplier").status 
                    = Option.GetBoolOption(options[index]) ? Option.Status.Normal : Option.Status.Disabled;
            }
            return 0;
        }

        public override void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {
            MapModel map = editor.Map();
            GameModel data = editor.DataModel();
            bool debug = Option.GetBoolOption(options, "Debug");

            List<Grace> races = data.GetAllT<Grace>();
            Grace neutral = null;
            foreach (Grace race in races)
            {
                if (!race.playable)
                {
                    neutral = race;
                    break;
                }
            }
            string neutralId = "PL0000";
            List<D2Player> players = map.GetListByType<D2Player>();
            foreach (D2Player player in players)
            {
                if (player._raceId == neutral.race_id)
                {
                    neutralId = player.ObjId();
                    break;
                }
            }

            bool neutralStacks = Option.GetBoolOption(options, "NeutralStacks");
            bool playableRacesStacks = Option.GetBoolOption(options, "PlayableRacesStacks");
            float playableRacesStacksMult = Option.GetFloatOption(options, "PlayableRaceStacksExtraMultiplier");

            bool stacksItems = Option.GetBoolOption(options, "StacksItems");
            bool villageVisiters = Option.GetBoolOption(options, "VillageVisiters");
            bool villageGarrison = Option.GetBoolOption(options, "VillageGarrison");
            bool villageItems = Option.GetBoolOption(options, "VillageItems");
            bool stackTemplates = Option.GetBoolOption(options, "StackTemplates");

            List<string> ignored = new List<string>();
            List<D2Capital> capitals = map.GetListByType<D2Capital>();
            foreach (D2Capital capital in capitals)
                ignored.Add(capital._Stack);

            if (!villageVisiters)
            {
                List<D2Village> villages = map.GetListByType<D2Village>();
                foreach (D2Village village in villages)
                    ignored.Add(village._Stack);
            }
            List<string> objectsToRemove = new List<string>();
            List<D2Stack> stacks = map.GetListByType<D2Stack>();
            foreach (D2Stack stack in stacks)
            {
                if (ignored.Contains(stack.ObjId()))
                    continue;
                if (stack._owner == neutralId && !neutralStacks)
                    continue;
                if(stack._owner != neutralId && !playableRacesStacks)
                    continue;

                foreach (string unitid in stack._Unit)
                {
                    if (unitid != "000000")
                        objectsToRemove.Add(unitid);
                }

                if (stacksItems)
                {
                    foreach (string itemid in stack._items)
                        objectsToRemove.Add(itemid);
                }

                StackTemplate template = converter.ToStackTemplate(stack);
                template.playableOwner = stack._owner != neutralId;
                template.extraData = stack._objId;
                if (debug)
                {
                    Logger.Log("Before: ");
                    Logger.Log(template.ToString(data));
                }

                StackTemplate result;
                if (stack._owner == neutralId)
                    result = regenerator.RegenerateStack(ref template);
                else
                    result = regenerator.RegenerateStack(ref template, playablePowerMult);

                if (debug)
                {
                    Logger.Log("After: ");
                    Logger.Log(result.ToString(data));
                }

                stack._aiorder = result.aiOrder;
                stack._aiordertar = result.aiOrderTarget;
                stack._order = result.order;
                stack._orderTarg = result.orderTarget;

                for (int i = 0; i < 6; i++)
                {
                    stack._Pos[i] = result._Pos[i];
                    if (result._Unit[i] != "G000000000")
                    {
                        D2Unit d2unit = editor.AddUnit(result._Unit[i].ToLower(),
                                                                 result._UnitLvl[i]);

                        foreach (string modId in result._Mods[i])
                            d2unit._Mods.Add(modId);

                        if (result._LeaderIndex == i)
                        {
                            d2unit._Name = result.name;
                            stack._leaderId = d2unit.ObjId();
                        }
                        stack._Unit[i] = d2unit.ObjId();
                    }
                    else
                    {
                        stack._Unit[i] = "000000";
                    }
                }
                if (stacksItems)
                {
                    stack._items.Clear();
                    foreach (string item in result._Items)
                    {
                        stack._items.Add(editor.AddItem(item).ObjId());
                    }
                }

                map.ReplaceById(stack.ObjId(), stack);
            }

            if (villageGarrison)
            {
                List<D2Village> villages = map.GetListByType<D2Village>();

                foreach (D2Village village in villages)
                {
                    if (village._Owner == neutralId && !neutralStacks)
                        continue;
                    if (village._Owner != neutralId && !playableRacesStacks)
                        continue;
                    foreach (string unitid in village._Unit)
                    {
                        if (unitid != "000000")
                        {
                            objectsToRemove.Add(unitid);
                        }
                    }

                    StackTemplate template = converter.ToStackTemplate(village);
                    template.playableOwner = village._Owner != neutralId;
                    if (debug)
                    {
                        Logger.Log("Before: ");
                        Logger.Log(template.ToString(data));
                    }

                    StackTemplate result;
                    if (village._Owner == neutralId)
                        result = regenerator.RegenerateStack(ref template);
                    else
                        result = regenerator.RegenerateStack(ref template, playablePowerMult);

                    if (debug)
                    {
                        Logger.Log("After: ");
                        Logger.Log(result.ToString(data));
                    }
                    for (int i = 0; i < 6; i++)
                    {
                        village._Pos[i] = result._Pos[i];
                        if (result._Unit[i] != "G000000000")
                        {
                            D2Unit d2unit = editor.AddUnit(result._Unit[i].ToLower(),
                                                                 result._UnitLvl[i]);

                            foreach (string modId in result._Mods[i])
                                d2unit._Mods.Add(modId);

                            village._Unit[i] = d2unit.ObjId();
                        }
                        else
                        {
                            village._Unit[i] = "000000";
                        }
                    }

                    if (villageItems)
                    {
                        foreach (string itemid in village._Items)
                            objectsToRemove.Add(itemid);
                        village._Items.Clear();
                        foreach (string item in result._Items)
                        {
                            village._Items.Add(editor.AddItem(item).ObjId());
                        }
                    }
                    map.ReplaceById(village._ObjId, village);
                }
            }
            if (stackTemplates)
            {
                List<D2StackTemplate> objects = map.GetListByType<D2StackTemplate>();

                foreach (D2StackTemplate stack in objects)
                {
                    if (stack._Owner == neutralId && !neutralStacks)
                        continue;
                    if (stack._Owner != neutralId && !playableRacesStacks)
                        continue;
                    StackTemplate template = converter.ToStackTemplate(stack);
                    template.playableOwner = stack._Owner != neutralId;
                    template.extraData = stack._ObjId;
                    if (debug)
                    {
                        Logger.Log("Before: ");
                        Logger.Log(template.ToString(data));
                    }

                    StackTemplate result;
                    if (stack._Owner == neutralId)
                        result = regenerator.RegenerateStack(ref template);
                    else
                        result = regenerator.RegenerateStack(ref template, playablePowerMult);

                    if (debug)
                    {
                        Logger.Log("After: ");
                        Logger.Log(result.ToString(data));
                    }
                    stack._Order = result.order;
                    stack._OrderTarg = result.orderTarget;

                    stack._Mods.Clear();
                    for (int i = 0; i < 6; i++)
                    {
                        stack._Pos[i] = result._Pos[i];
                        stack._Unit[i] = result._Unit[i];
                        stack._UnitLvl[i] = result._UnitLvl[i];

                        if (result._LeaderIndex == i)
                        {
                            stack._Name = result.name;
                            stack._Leader = result._Unit[i];
                            stack._LeaderLvl = result._UnitLvl[i];
                        }

                        if (result._Pos[i] != -1)
                        {
                            foreach (string mod in result._Mods[result._Pos[i]])
                                stack._Mods.Add(new D2StackTemplate.UnitTemplateMod() { _UnitNum = i, _Modid = mod });
                        }
                    }
                    stack._Name = result.name;
                    map.ReplaceById(stack.ObjId(), stack);
                }
            }

            editor.RemoveObjects(objectsToRemove);
        }
    }
}
