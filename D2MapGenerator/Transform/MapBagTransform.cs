﻿using NevendaarTools.MapUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.Transform
{
    public class MapBagTransform : BaseTransform
    {
        public MapBagTransform()
        {
            Init("Bag", "all bags and chests on map");
        }

        public override void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {
            MapModel map = editor.Map();
            GameModel data = editor.DataModel();
            bool debug = Option.GetBoolOption(options, "Debug");

            List<D2Bag> _ItemsToReplace = map.GetListByType<D2Bag>();
            List<string> objectsToRemove = new List<string>();
            foreach (D2Bag bag in _ItemsToReplace)
            {
                List<string> itemTypes = new List<string>();
                foreach (string itemid in bag._items)
                {
                    D2Item item = map.GetById<D2Item>(itemid);
                    itemTypes.Add(item._type);
                    objectsToRemove.Add(itemid);
                }
                if (debug)
                {
                    Logger.Log("Before: " + "Bag_" + bag._posX.ToString() + "_" + bag._posY.ToString());
                    string items = "";
                    foreach (string itemid in itemTypes)
                    {
                        GItem item = data.GetObjectT<GItem>(itemid);
                        items += item.name_txt.value.text + " ";
                    }
                    Logger.Log(items);
                }
                List<string> newItems = regenerator.RegenerateItems(ref itemTypes, bag._posX, bag._posY);
                if (debug)
                {
                    Logger.Log("After: " + "Bag_" + bag._posX.ToString() + "_" + bag._posY.ToString());
                    string items = "";
                    foreach (string itemid in newItems)
                    {
                        GItem item = data.GetObjectT<GItem>(itemid);
                        items += item.name_txt.value.text + " ";
                    }
                    Logger.Log(items);
                }
                bag._items.Clear();
                foreach(string item in newItems)
                {
                    bag._items.Add(editor.AddItem(item).ObjId());
                }

                map.ReplaceById(bag.ObjId(), bag);
            }
            editor.RemoveObjects(objectsToRemove);
        }
    }
}
