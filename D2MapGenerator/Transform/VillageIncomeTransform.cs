﻿using NevendaarTools;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;
using NevendaarTools.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace D2MapEdit.Transform
{
    class VillageIncomeTransform : BaseTransform
    {
        public VillageIncomeTransform()
        {
            Init("VillageIncome", "map");
            AddOption("T1 village income", 20);
            AddOption("T2 village income", 30);
            AddOption("T3 village income", 40);
            AddOption("T4 village income", 50);
            AddOption("T5 village income", 75);

            AddOption("Capital income", 0);
            bool tmp = false;
            SetOption(0, "False", ref tmp);
        }

        public override void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {
            MapModel map = editor.Map();
            GameModel data = editor.DataModel();
            bool debug = Option.GetBoolOption(options, "Debug");

            for (int i = 0; i < 6; ++i)
            {
                string name = "TIER_" + i.ToString() + "_CITY_INCOME";
                if (!editor.hasVariable(name))
                {
                    editor.AddVariable(name, 0);
                }
            }

            editor.SetVariable(ref map, "TIER_0_CITY_INCOME", Option.GetIntOption(options, "Capital income"));
            for (int i = 1; i < 6; ++i)
            {
                string name = "TIER_" + i.ToString() + "_CITY_INCOME";
                string propName = "T" + i.ToString() + " village income";
                editor.SetVariable(ref map, name, Option.GetIntOption(options, propName));
            }
        }
    }
}
