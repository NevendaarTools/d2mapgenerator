﻿using System;
using System.Collections.Generic;
using System.Text;
using NevendaarTools.DataTypes;
using NevendaarTools.MapUtils;

namespace NevendaarTools.Transform
{
    public class MerchantTransform : BaseTransform
    {
        //config
        public bool fullTypeCompliance = true;
        public int alwaysReviveCount = 0;//G000IG0001
        public int alwaysHealCount = 0;//G000IG0006
        //local data
        List<D2Merchant> _ItemsToReplace = new List<D2Merchant>();

        public MerchantTransform()
        {
            Init("Merchant", "all merchants on map");
            AddOption("Trader", true);
            AddOption("FullTypeCompliance", true);
            AddOption("AlwaysReviveCount", 0);
            AddOption("AlwaysHealCount", 0);

            AddOption("Mercenary", false);
            AddOption("MercenaryRaceCompliance", true);
        }

        public override int SetOption(int index, string value, ref bool neadReload)
        {
            base.SetOption(index, value, ref neadReload);
            if (options[index].name == "Trader")
            {
                Option.Status enabled = Option.GetBoolOption(options[index]) ? Option.Status.Normal : Option.Status.Disabled;
                Option.GetOption(options, "FullTypeCompliance").status = enabled;
                Option.GetOption(options, "AlwaysReviveCount").status = enabled;
                Option.GetOption(options, "AlwaysHealCount").status = enabled;
            }
            if (options[index].name == "Mercenary")
            {
                Option.Status enabled = Option.GetBoolOption(options[index]) ? Option.Status.Normal : Option.Status.Disabled;
                Option.GetOption(options, "MercenaryRaceCompliance").status = enabled;
            }
            return 0;
        }


        public override void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {
            MapModel map = editor.Map();
            GameModel data = editor.DataModel();
            bool debug = Option.GetBoolOption(options, "Debug");

            bool trader = Option.GetBoolOption(options, "Trader");
            bool fullTypeCompliance = Option.GetBoolOption(options, "FullTypeCompliance");
            int  alwaysReviveCount = Option.GetIntOption(options, "AlwaysReviveCount");
            int  alwaysHealCount = Option.GetIntOption(options, "AlwaysHealCount");
            bool mercenary = Option.GetBoolOption(options, "Mercenary");
            bool mercenaryRaceCompliance = Option.GetBoolOption(options, "MercenaryRaceCompliance");

            if (trader)
            {
                List<D2Merchant> traders = map.GetListByType<D2Merchant>();

                foreach (D2Merchant merch in traders)
                {
                    if (debug)
                    {
                        Logger.Log("Before: " + merch._Title + "_" + merch._PosX.ToString() + "_" + merch._PosY.ToString());
                        string oldItems = "";
                        foreach (D2Merchant.MerchantItemEntry item in merch._Items)
                        {
                            GItem gitem = data.GetObjectT<GItem>(item._ItemId);
                            oldItems += gitem.name_txt.value.text + "_" + item._ItemCount.ToString() + " \n";
                        }
                        Logger.Log(oldItems);
                    }
                    merch._Items = regenerator.RegenerateMerchantItem(ref merch._Items, fullTypeCompliance ? 4 : 2);
                    bool healFound = false;
                    bool reviveFound = false;
                    for (int i = 0; i < merch._Items.Count; i++)
                    {
                        if (merch._Items[i]._ItemId == "G000IG0006" && alwaysHealCount > 0)//heal
                        {
                            healFound = true;
                            merch._Items[i]._ItemCount = alwaysHealCount;
                        }
                        if (merch._Items[i]._ItemId == "G000IG0001" && alwaysReviveCount > 0)//revive
                        {
                            reviveFound = true;
                            merch._Items[i]._ItemCount = alwaysReviveCount;
                        }
                    }
                    if (alwaysHealCount > 0 && !healFound)
                    {
                        merch._Items.Add(new D2Merchant.MerchantItemEntry() { _ItemId = "G000IG0006", _ItemCount = alwaysHealCount });
                    }
                    if (alwaysReviveCount > 0 && !reviveFound)
                    {
                        merch._Items.Add(new D2Merchant.MerchantItemEntry() { _ItemId = "G000IG0001", _ItemCount = alwaysReviveCount });
                    }
                    if (debug)
                    {
                        Logger.Log("After: " + merch._Title + "_" + merch._PosX.ToString() + "_" + merch._PosY.ToString());
                        string oldItems = "";
                        foreach (D2Merchant.MerchantItemEntry item in merch._Items)
                        {
                            GItem gitem = data.GetObjectT<GItem>(item._ItemId);
                            oldItems += gitem.name_txt.value.text + "_" + item._ItemCount.ToString() + " \n";
                        }
                        Logger.Log(oldItems);
                    }

                    map.ReplaceById(merch._objId, merch);
                }
            }
            if (mercenary)
            {
                List<D2Mercs> mercs = map.GetListByType<D2Mercs>();

                foreach (D2Mercs merch in mercs)
                {
                    if (debug)
                    {
                        Logger.Log("Before: " + merch._Title + "_" + merch._PosX.ToString() + "_" + merch._PosY.ToString());
                        string oldItems = "";
                        foreach (D2Mercs.MercsUnitEntry item in merch._Units)
                        {
                            Gunit gitem = data.GetObjectT<Gunit>(item._UnitId);
                            oldItems += gitem.name_txt.value.text + "_" + item._UnitLevel.ToString() + "_" + item._UnitUniq.ToString() + " \n";
                        }
                        Logger.Log(oldItems);
                    }

                    merch._Units = regenerator.RegenerateMercsUnits(ref merch._Units, mercenaryRaceCompliance ? 4 : 2);
                    if (debug)
                    {
                        Logger.Log("After: " + merch._Title + "_" + merch._PosX.ToString() + "_" + merch._PosY.ToString());
                        string oldItems = "";
                        foreach (D2Mercs.MercsUnitEntry item in merch._Units)
                        {
                            Gunit gitem = data.GetObjectT<Gunit>(item._UnitId);
                            oldItems += gitem.name_txt.value.text + "_" + item._UnitLevel.ToString() + "_" + item._UnitUniq.ToString() + " \n";
                        }
                        Logger.Log(oldItems);
                    }

                    map.ReplaceById(merch._objId, merch);
                }
            }
        }
    }
}
