﻿using System;
using System.Collections.Generic;
using System.Text;
using NevendaarTools;
using System.IO;
using D2MapEdit;
using NevendaarTools.MapUtils;
using SimpleJSON;

namespace NevendaarTools.Transform
{
    public class MapTransformExecutor : OptionsStorage
    {
        public List<BaseTransform> _Transforms = new List<BaseTransform>();

        private MapReader _MapReader = new MapReader();
        public GameModel _GameModel = new GameModel();
        public MapModel _MapModel = new MapModel();
        private MapEditor editor = new MapEditor();
        MapObjectconverter _converter = new MapObjectconverter();
        MapObjectRegenerator regenerator = new DummyRegenerator();


        public void RestoreSettings(string path)
        {
            bool neadReload = false;
            JSONObject json = JsonHelper.Load(path);
            if (json == null)
            {
                Logger.LogError("Faileed to restore settings:" + path);
                return;
            }
            OptionsPreset preset = new OptionsPreset();
            preset.name = "LastSettings";
            foreach (JSONObject record in json["options"].AsArray)
            {
                OptionsPreset.OptionValue val = new OptionsPreset.OptionValue();
                val.name = record["name"];
                val.group = record["group"];
                val.value = record["value"];
                preset.values.Add(val);
            }
            foreach (OptionsPreset.OptionValue val in preset.values)
            {
                for (int i = 0; i < OptionsCount(); ++i)
                {
                    Option option = GetOptionAt(i);
                    if (option.name == val.name && option.group == val.group)
                    {
                        SetOption(i, val.value, ref neadReload);
                        break;
                    }
                }
            }
        }

        public void SaveSettings(string path)
        {
            JSONObject preset = new JSONObject();
            SimpleJSON.JSONArray presetOptions = new JSONArray();

            for (int i = 0; i < OptionsCount(); ++i)
            {
                Option option = GetOptionAt(i);
                JSONObject presetValue = new JSONObject();
                presetValue["group"] = option.group;
                presetValue["name"] = option.name;
                presetValue["value"] = option.value;
                presetOptions.Add(presetValue);
            }

            preset["options"] = presetOptions;
            JsonHelper.Save(path, preset);
        }

        public void LoadGameData(string path, ref SettingsManager manager)
        {
            _GameModel.Load(path, true);
        }

        public void SetRegenerator(MapObjectRegenerator regen)
        {
            regenerator = regen;
        }

        public void LoadMap(string path)
        {
            Logger.Log("Parsing map: " + path);
            if (_MapReader.ReadMap(_MapModel, path))
            {
                Logger.Log("Finished: Parsing map. Success!");
            }
            else
                Logger.Log("Finished: Parsing map. Failed!");
        }

        public void SaveMap(string path)
        {
            Logger.Log("Saving map: " + path);
            if (MapReader.SaveMap(_MapModel, path))
            {
                Logger.Log("Success");
            }
            else
                Logger.Log("Failed");
        }

        public bool Execute()
        {
            editor = new MapEditor();
            editor.Init(ref _MapModel, ref _GameModel);
            _converter.Init(ref _MapModel);
            regenerator.Configure(editor);

            foreach (BaseTransform transform in _Transforms)
            {
                if (!transform.Enabled())
                    continue;
                editor.ClearIds();
                transform.Process(editor, regenerator, _converter);
            }
            if (!_MapModel.Header._Name.EndsWith("_gen"))
                editor.Rename(_MapModel.Header._Name += "_gen", _MapModel.Header._Author);
            editor.Finish();
            regenerator.Finish(_MapModel);

            return true;
        }

        public int OptionsCount()
        {
            int result = regenerator.Options().OptionsCount();
            foreach (BaseTransform transform in _Transforms)
            {
                result += transform.OptionsCount();
            }
            return result;
        }

        public int SetOption(int index, string value, ref bool neadReload)
        {
            int result = index;
            if (regenerator.Options().OptionsCount() > result)
                return regenerator.Options().SetOption(result, value, ref neadReload);
            result -= regenerator.Options().OptionsCount();
            foreach (BaseTransform transform in _Transforms)
            {
                if (transform.OptionsCount() > result)
                    return transform.SetOption(result, value, ref neadReload);
                result -= transform.OptionsCount();
            }
            return 1;
        }

        public Option GetOptionAt(int index)
        {
            int result = index;
            if (regenerator.Options().OptionsCount() > result)
                return regenerator.Options().GetOptionAt(result);
            result -= regenerator.Options().OptionsCount();
            foreach (BaseTransform transform in _Transforms)
            {
                if (transform.OptionsCount() > result)
                    return transform.GetOptionAt(result);
                result -= transform.OptionsCount();
            }
            return null;
        }
    }
}
