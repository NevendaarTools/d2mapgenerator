﻿using NevendaarTools.MapUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.Transform
{
    class RuinTransform : BaseTransform
    {
        //config
        public bool regenUnits = true;
        public bool regenItem = true;
        public bool removeItem = false;
        public bool genItemIfEmpty = true;
        public float itemCostModIdEmpty = 1.0f;
        public bool addItemCostDeltaToReward = true;
        public float _ManaChance = 0.0f;
        public float _ManaValue = 0.5f;

        //local data
        List<D2Ruin> _ObjectsToReplace = new List<D2Ruin>();
        Dictionary<string, StackTemplate> _Templates = new Dictionary<string, StackTemplate>();
        List<string> _ObjectsToRemove = new List<string>();

        public RuinTransform()
        {
            Init("Ruin", "the ruins");

            AddOption("RegenerateUnits", true);
            AddOption("RegenerateReward", true);
            AddOption("CreateItemIfEmpty", true);
            AddOption("ConvertToManaChance", 0);
            AddOption("ConvertToManaValue", 0.5f);
        }

        public override int SetOption(int index, string value, ref bool neadReload)
        {
            base.SetOption(index, value, ref neadReload);
            if (options[index].name == "RegenerateReward")
            {
                bool regenerateReward = Option.GetBoolOption(options, "RegenerateReward");
                Option.GetOption(options, "ConvertToManaChance").status
                    = regenerateReward ? Option.Status.Normal : Option.Status.Disabled;
                Option.GetOption(options, "ConvertToManaValue").status
                    = regenerateReward ? Option.Status.Normal : Option.Status.Disabled;
            }
            return 0;
        }

        public override void Process(MapEditor editor, MapObjectRegenerator regenerator, MapObjectconverter converter)
        {
            MapModel map = editor.Map();
            GameModel data = editor.DataModel();
            bool debug = Option.GetBoolOption(options, "Debug");

            bool regenerateUnits = Option.GetBoolOption(options, "RegenerateUnits");
            bool regenerateItem = Option.GetBoolOption(options, "RegenerateReward");
            bool createItemIfEmpty = Option.GetBoolOption(options, "CreateItemIfEmpty");

            int convertToManaChance = Option.GetIntOption(options, "ConvertToManaChance");
            float convertToManaValue = Option.GetFloatOption(options, "ConvertToManaValue");


            List<D2Ruin> objects = map.GetListByType<D2Ruin>();

            foreach (D2Ruin ruin in objects)
            {
                if (!regenerateUnits)
                    continue;

                foreach (string unitid in ruin._unit)
                {
                    if (unitid != "000000")
                    {
                        _ObjectsToRemove.Add(unitid);
                    }
                }
                StackTemplate template = converter.ToStackTemplate(ruin);
                if (debug)
                {
                    Logger.Log("Before: ");
                    Logger.Log(template.ToString(data));
                }
                StackTemplate result = regenerator.RegenerateStack(ref template);
                if (debug)
                {
                    Logger.Log("After: ");
                    Logger.Log(result.ToString(data));
                }
                for (int i = 0; i < 6; i++)
                {
                    ruin._pos[i] = result._Pos[i];
                    if (result._Unit[i] != "G000000000")
                    {
                        D2Unit d2unit = editor.AddUnit(result._Unit[i].ToLower(),
                                                                 result._UnitLvl[i]);
                        foreach (string modId in result._Mods[i])
                            d2unit._Mods.Add(modId);

                        ruin._unit[i] = d2unit.ObjId();
                    }
                    else
                    {
                        ruin._unit[i] = "000000";
                    }
                }
            }
            if (regenerateItem)
            {
                foreach (D2Ruin ruin in objects)
                {
                    string itemBefore = ruin._item;
                    string costBefore = ruin._cash;

                    if (createItemIfEmpty && ruin._item == "G000000000")
                        ruin._item = regenerator.GenerateItem(ruin._cash, ruin._posX, ruin._posY, itemCostModIdEmpty);
                    if (ruin._item == "")
                        ruin._item = "G000000000";

                    if (ruin._item != "G000000000")
                    {
                        RuinReward reward = regenerator.RegenerateRuinReward(ruin._item, ruin._cash, addItemCostDeltaToReward, ruin._posX, ruin._posY, _ManaChance, _ManaValue);
                        ruin._item = reward.item;
                        ruin._cash = reward.cash;
                    }
                    if (ruin._item == "")
                        ruin._item = "G000000000";

                    if (debug)
                    {
                        string before = "G000000000";
                        if (itemBefore != "G000000000")
                        {
                            GItem beforeItem = data.GetObjectT<GItem>(itemBefore);
                            before = beforeItem.name_txt.value.text;
                        }
                        if (ruin._item != "G000000000")
                        {
                            GItem afterItem = data.GetObjectT<GItem>(ruin._item);
                            Logger.Log(ruin._name + " " + before + "(" + itemBefore + ") -> " + afterItem.name_txt.value + "(" + ruin._item + ")");
                        }
                        else
                            Logger.Log(ruin._name + " " + before + "(" + itemBefore + ") -> G000000000");

                        Logger.Log("Cost: " + costBefore + " -> " + ruin._cash);
                        Logger.Log("");
                    }

                    map.ReplaceById(ruin.ObjId(), ruin);
                }
            }
        }
    }
}
