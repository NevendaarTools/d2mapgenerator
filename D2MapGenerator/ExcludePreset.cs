﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace NevendaarTools
{
    public class Preset
    {
        public string name;
        public string desc = "";
        public List<string> ids = new List<string>();
    }
    public class PresetManager
    {
        public delegate void LogDelegate(string text);
        public LogDelegate _LogDelegate;
        public void Log(string str)
        {
            _LogDelegate?.Invoke(str);
        }

        public Dictionary<string, Preset> _Presets = new Dictionary<string, Preset>();

        public static List<string> GetPresetIdsFromFile(string path, LogDelegate log)
        {
            List<string> result = new List<string>();
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    
                    String line = sr.ReadToEnd();
                    string[] lines = line.Split(
                        new[] { "\r\n", "\r", "\n" },
                        StringSplitOptions.None
                    );
                    foreach (string str in lines)
                    {
                        if (str.StartsWith("#") || str == "")
                            continue;
                        string linestr = str;
                        while (linestr.Contains("\t\t"))
                            linestr = linestr.Replace("\t\t", "\t");
                        result.Add(linestr);
                    }
                }
            }
            catch (Exception e)
            {
                log?.Invoke("Failed to open " + path);
            }
            return result;
        }

        public bool LoadPreset(string name, string path)
        {
            if (_Presets.ContainsKey(name))
                return false;
            Preset preset = new Preset();
            preset.name = name;
            preset.ids = GetPresetIdsFromFile(path, _LogDelegate);
            _Presets.Add(name, preset);

            return true;
        }

        public void LoadPresets()
        {
            try
            {
                IEnumerable<string> allfiles = Directory.EnumerateFiles("Resources/", "ExcludeItemIDs_*");
                foreach (string filename in allfiles)
                {
                    string name = System.IO.Path.GetFileNameWithoutExtension(filename);
                    name = name.Replace("ExcludeItemIDs_", "");
                    if (name == "")
                        name = "Default";
                    LoadPreset(name, filename);
                }
                LoadPreset("Default", "Resources/ExcludeIDs.txt");
            }
            catch (Exception e)
            {
                Preset preset = new Preset();
                preset.name = "Default";
                _Presets.Add(preset.name, preset);
                Log("Failed to load presets from Resources folder!");
            }
            
        }

        public bool SavePreset(string name, List<string> ids)
        {
            Preset preset;
            if (_Presets.ContainsKey(name))
            {
                preset = _Presets[name];
                preset.ids.Clear();
                foreach (string key in ids)
                {
                    _Presets[name].ids.Add(key);
                }
            }
            else
            {
                preset = new Preset();
                preset.name = name;
                foreach (string key in ids)
                {
                    preset.ids.Add(key);
                }

                _Presets.Add(name, preset);
            }

            try
            {
                System.IO.StreamWriter writer = new System.IO.StreamWriter("Resources/ExcludeItemIDs_" + name + ".txt");
                foreach (string id in preset.ids)
                {
                    writer.WriteLine(id);
                }
                writer.Close();
            }
            catch (Exception e)
            {
                Log("Failed to write preset: " + name);
                Log(e.Message);
                return false;
            }

            return true;
        }
    }

    public class PresetController
    {
        public string name;
        public bool enabled;
        public CheckBox checkBox;

        public ExcludeSelectWidget parent;

        public void onClicked(object sender, EventArgs e)
        {
            enabled = checkBox.Checked;
            parent.UpdatePresetState(name);
        }
    }
}
