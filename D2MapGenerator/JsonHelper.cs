﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using SimpleJSON;
using NevendaarTools.Transform;
using NevendaarTools;

class JsonHelper
{
    public static JSONObject Load(string path)
    {
        string text = "";
        try
        {
            text = File.ReadAllText(path);
        }
        catch {
            return null;
        }

        return (JSONObject)JSON.Parse(text);
    }

    public static void Save(string path, JSONObject obj)
    {
        File.WriteAllText(path, obj.ToString(4));
    }

    public static JSONObject JsonFromDict(Dictionary<string, string> dict)
    {
        JSONObject result = new JSONObject();
        foreach (string key in dict.Keys)
        {
            result.Add(key, dict[key]);
        }
        return result;
    }

    public static JSONObject JsonFromDict(Dictionary<string, float> dict)
    {
        JSONObject result = new JSONObject();
        foreach (string key in dict.Keys)
        {
            result.Add(key, dict[key]);
        }
        return result;
    }

    public static Dictionary<string, string> JsonToStringDict(JSONObject inObject)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (string key in inObject.Keys)
        {
            result.Add(key, inObject[key]);
        }
        return result;
    }

    public static JSONObject JsonFromStringList(List<string> dict)
    {
        JSONObject result = new JSONObject();
        for (int i = 0; i < dict.Count; ++i)
        {
            result[i.ToString()] = dict[i];
        }
        return result;
    }

    public static JSONArray JsonArrayFromStringList(List<string> dict)
    {
        JSONArray result = new JSONArray();
        for (int i = 0; i < dict.Count; ++i)
        {
            result.Add(dict[i]);
        }
        return result;
    }

    public static List<string> StringListFromJsonDict(JSONObject inObject)
    {
        List<string> result = new List<string>();
        for (int i = 0; i < inObject.Count; ++i)
        {
            result.Add(inObject[i.ToString()]);
        }
        return result;
    }

    public static List<string> StringListFromJsonArray(JSONArray inObject)
    {
        List<string> result = new List<string>();
        for (int i = 0; i < inObject.Count; ++i)
        {
            result.Add(inObject[i]);
        }
        return result;
    }

    public static Dictionary<string, float> JsonToFloatDict(JSONObject inObject)
    {
        Dictionary<string, float> result = new Dictionary<string, float>();
        foreach (string key in inObject.Keys)
        {
            result.Add(key, inObject[key].AsFloat);
        }
        return result;
    }

    public static List<string> StringListFromJsonString(string inObject)
    {
        List<string> result = new List<string>();
        string [] splited = inObject.Split(',');
        foreach (string value in splited)
        {
            result.Add(value.Trim());
        }
        return result;
    }

    public static List<KeyValuePair<string, string>> StringPairListFromJsonArray(JSONArray inArray)
    {
        List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
        foreach (JSONObject inObject in inArray)
        {
            result.Add(new KeyValuePair<string, string>(inObject["key"], inObject["value"]));
        }
        return result;
    }

    public static List<KeyValuePair<string, int>> StringIntPairListFromJsonArray(JSONArray inArray)
    {
        List<KeyValuePair<string, int>> result = new List<KeyValuePair<string, int>>();
        foreach (JSONObject inObject in inArray)
        {
            result.Add(new KeyValuePair<string, int>(inObject["key"], inObject["value"]));
        }
        return result;
    }

    public static JSONArray JsonArrayToStringPairList(List<KeyValuePair<string, string>> list)
    {
        JSONArray result = new JSONArray();
        foreach (KeyValuePair<string, string> inObject in list)
        {
            JSONObject obj = new JSONObject();
            obj.Add("key", inObject.Key);
            obj.Add("value", inObject.Value);
            result.Add(obj);
        }
        return result;
    }

    public static JSONArray JsonArrayToStringPairList(List<KeyValuePair<string, int>> list)
    {
        JSONArray result = new JSONArray();
        foreach (KeyValuePair<string, int> inObject in list)
        {
            JSONObject obj = new JSONObject();
            obj.Add("key", inObject.Key);
            obj.Add("value", inObject.Value);
            result.Add(obj);
        }
        return result;
    }

    public static JSONObject OptionToJson(Option option)
    {
        JSONObject result = new JSONObject();
        result["name"] = option.name;
        result["desc"] = option.desc;
        result["value"] = option.value;
        result["min"] = option.min;
        result["max"] = option.max;
        if (option.variants != null)
            result["variants"] = JsonArrayFromStringList(option.variants);
        if (option.variantValues != null)
            result["variantValues"] = JsonArrayFromStringList(option.variantValues);
        result["status"] = (int)option.status;
        result["type"] = (int)option.type;
        return result;
    }

    public static Option OptionFromJson(JSONObject json)
    {
        Option result = new Option();
        result.name = json["name"];
        if (json.HasKey("desc"))
            result.desc = json["desc"];
        result.value = json["value"];
        if (json.HasKey("min"))
            result.min = json["min"];
        if (json.HasKey("max"))
            result.max = json["max"];
        if (json.HasKey("variants"))
            result.variants = StringListFromJsonArray(json["variants"].AsArray);
        if (json.HasKey("variantValues"))
            result.variantValues = StringListFromJsonArray(json["variantValues"].AsArray);
        result.status = (Option.Status)json["status"].AsInt;
        result.type = (Option.Type)json["type"].AsInt;
        return result;
    }
}

