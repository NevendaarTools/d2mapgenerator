﻿using NevendaarTools;
using NevendaarTools.Transform;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace D2MapEdit
{
    public class BoolOptionControl : CheckBox
    {
        private int index;
        private OptionsStorage storage;
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            storage.SetOption(index, (this.Checked) ? "True" : "False", ref reload);
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public BoolOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);

            Text = TranslationHelper.Instance().Tr(option.name);
            Width = w;
            Height = 20;
            toolTip1.SetToolTip(this, TranslationHelper.Instance().Tr(option.desc));
            Checked = option.value.Trim() == "True";
            CheckStateChanged += ValueChanged;
        }
    }

    public class NumericOptionControl : FlowLayoutPanel
    {
        private int index;
        private OptionsStorage storage;
        private NumericUpDown control;
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            storage.SetOption(index, control.Value.ToString(), ref reload);
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public NumericOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);

            Margin = new Padding(0);
            FlowDirection = FlowDirection.LeftToRight;
            Width = w;
            Height = 25;
            control = new NumericUpDown();
            if (option.type == Option.Type.Float)
            {
                control.DecimalPlaces = 2;
                control.Increment = 0.05M;
                string tmp = option.min;
                tmp = tmp.Replace(",", ".");
                tmp = tmp.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                control.Minimum = Decimal.Parse(tmp);
                tmp = option.max;
                tmp = tmp.Replace(",", ".");
                tmp = tmp.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                control.Maximum = Decimal.Parse(tmp);
            }
            else
            {
                control.DecimalPlaces = 0;
                control.Increment = 1M;
                control.Minimum = Decimal.Parse(option.min);
                control.Maximum = Decimal.Parse(option.max);
            }

            string value = option.value;
            value = value.Replace(",", ".");
            value = value.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            control.Value = decimal.Parse(value);
            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;
            //nameLabel.AutoSize = true;
            nameLabel.Width = 200;
            nameLabel.Height = control.Height;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
            Controls.Add(nameLabel);
            Controls.Add(control);
            control.ValueChanged += ValueChanged;
        }
    }
    public class StringOptionControl : FlowLayoutPanel
    {
        private int index;
        private OptionsStorage storage;
        private TextBox control;
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            storage.SetOption(index, control.Text, ref reload);
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public StringOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);
            Width = w;
            Height = 25;
            Margin = new Padding(0);
            FlowDirection = FlowDirection.LeftToRight;

            control = new TextBox();
            control.Multiline = false;
            control.Text = option.value;
            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;
            nameLabel.Width = 200;
            nameLabel.Height = control.Height;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
            Controls.Add(nameLabel);
            Controls.Add(control);
            control.TextChanged += ValueChanged;
        }
    }
    public class EnumOptionControl : FlowLayoutPanel
    {
        private int index;
        private OptionsStorage storage;
        private ComboBox control;
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            Option option = storage.GetOptionAt(index);
            //if (option.variantValues == null || option.variantValues.Count == 0)
            storage.SetOption(index, option.variants[control.SelectedIndex], ref reload);
            //else
            //{
            //    int variantIndex = option.variants.IndexOf(control.Text);
            //    storage.SetOption(index, option.variantValues[variantIndex], ref reload);
            //}
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public EnumOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);
            Width = w;
            Height = 25;
            Margin = new Padding(0);
            FlowDirection = FlowDirection.LeftToRight;

            control =  new ComboBox();
            control.DropDownStyle = ComboBoxStyle.DropDownList;

            foreach (string variant in option.variants)
            {
                control.Items.Add(TranslationHelper.Instance().Tr(variant));
            }
            control.SelectedIndex = option.variants.IndexOf(option.value);

            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;
            
            nameLabel.Width = 200;
            nameLabel.Height = control.Height;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
            Controls.Add(nameLabel);
            control.Width = w - 20 - nameLabel.Width;
            Controls.Add(control);
            control.SelectedIndexChanged += ValueChanged;
        }
    }
    public class SemgaListOptionControl : Panel
    {
        private int index;
        private OptionsStorage storage;
        private Label nameLabel;
        private List<CheckBox> checkBoxes = new List<CheckBox>();
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            Option option = storage.GetOptionAt(index);

            string newValue = "";
            for(int i = 0; i < checkBoxes.Count; ++i)
            {
                if (checkBoxes[i].Checked)
                {
                    if (newValue != "")
                        newValue += ";";
                    newValue += option.variantValues[i];
                }
            }
            if (newValue == "")
                newValue = option.defValue;
            storage.SetOption(index, newValue, ref reload);
            nameLabel.Text = option.name + ": " +  newValue;
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public SemgaListOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);
            Width = w;
            Margin = new Padding(0);
            
            nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;

            nameLabel.Width = w;
            nameLabel.Height = 20;
            Height = 25;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
            Controls.Add(nameLabel);
            string[] currentVars = option.value.Split(';');
            for (int i = 0; i < option.variants.Count; ++i)
            {
                CheckBox box = new CheckBox();
                box.Width = w / 2 - 10;
                box.Text = option.variants[i];
                box.CheckStateChanged += ValueChanged;
                box.Checked = currentVars.Contains(option.variantValues[i]);
                box.Top = Height;
                //box.Top = resultPanel.Height;
                if (i % 2 != 0)
                {
                    box.Left = 5;
                    Height += box.Height + 3;
                }
                else
                    box.Left = w / 2 + 5;
                Controls.Add(box);
                checkBoxes.Add(box);   
            }
        }
    }

    public class PathOptionControl : Panel
    {
        private int index;
        private OptionsStorage storage;
        private TextBox control;
        public delegate void RequestUpdate();
        public RequestUpdate requestReload;
        public RequestUpdate requestUpdate;

        public void ValueChanged(object sender, EventArgs e)
        {
            bool reload = false;
            storage.SetOption(index, control.Text, ref reload);
            if (reload)
                requestReload?.Invoke();
            else
                requestUpdate?.Invoke();
        }

        public void ButtonClicked(object sender, EventArgs e)
        {
            Option option = storage.GetOptionAt(index);

            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = option.variantValues[0];
            openDialog.InitialDirectory = option.variantValues[1];
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;
            control.Text = openDialog.FileName;
        }

        public PathOptionControl(ref ToolTip toolTip1, ref OptionsStorage optStorage, int w, int optIndex)
        {
            storage = optStorage;
            index = optIndex;
            Option option = storage.GetOptionAt(optIndex);
            Width = w;
            Height = 45;
            Margin = new Padding(0);
            
            control = new TextBox();
            control.Multiline = false;
            control.Text = option.value;
            control.Top = 25;
            control.Left = 5;
            control.Width = w - 45;
            Label nameLabel = new Label();
            nameLabel.Text = TranslationHelper.Instance().Tr(option.name);
            nameLabel.TextAlign = ContentAlignment.BottomLeft;
            nameLabel.Width = 200;
            nameLabel.Height = control.Height;
            toolTip1.SetToolTip(nameLabel, TranslationHelper.Instance().Tr(option.desc));
            Button button = new Button();
            button.Text = "...";
            button.Top = 25;
            button.Left = w - 40;
            button.Width = 25;
            button.Click += ButtonClicked;
            Controls.Add(nameLabel);
            Controls.Add(control);
            Controls.Add(button);

            control.TextChanged += ValueChanged;
        }
    }
}
