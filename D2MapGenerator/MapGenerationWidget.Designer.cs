﻿namespace D2MapEdit
{
    partial class MapGenerationWidget
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapInfoWidget1 = new NevedaarWidgets.MapInfoWidget();
            this.logTextBox = new System.Windows.Forms.RichTextBox();
            this.buttonSaveGenMap = new System.Windows.Forms.Button();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // mapInfoWidget1
            // 
            this.mapInfoWidget1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapInfoWidget1.Location = new System.Drawing.Point(3, 29);
            this.mapInfoWidget1.Margin = new System.Windows.Forms.Padding(0);
            this.mapInfoWidget1.Name = "mapInfoWidget1";
            this.mapInfoWidget1.Size = new System.Drawing.Size(257, 384);
            this.mapInfoWidget1.TabIndex = 10;
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.logTextBox.Location = new System.Drawing.Point(3, 416);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(257, 203);
            this.logTextBox.TabIndex = 9;
            this.logTextBox.Text = "";
            // 
            // buttonSaveGenMap
            // 
            this.buttonSaveGenMap.Enabled = false;
            this.buttonSaveGenMap.Location = new System.Drawing.Point(189, 3);
            this.buttonSaveGenMap.Name = "buttonSaveGenMap";
            this.buttonSaveGenMap.Size = new System.Drawing.Size(71, 23);
            this.buttonSaveGenMap.TabIndex = 8;
            this.buttonSaveGenMap.Text = "Save";
            this.buttonSaveGenMap.UseVisualStyleBackColor = true;
            this.buttonSaveGenMap.Click += new System.EventHandler(this.buttonSaveGenMap_Click);
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Enabled = false;
            this.buttonGenerate.Location = new System.Drawing.Point(3, 3);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(71, 23);
            this.buttonGenerate.TabIndex = 7;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(266, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(3);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(452, 616);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // MapGenerationWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mapInfoWidget1);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.buttonSaveGenMap);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Name = "MapGenerationWidget";
            this.Size = new System.Drawing.Size(721, 622);
            this.ResumeLayout(false);

        }

        #endregion

        private NevedaarWidgets.MapInfoWidget mapInfoWidget1;
        private System.Windows.Forms.RichTextBox logTextBox;
        private System.Windows.Forms.Button buttonSaveGenMap;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
    }
}
