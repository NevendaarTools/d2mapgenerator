﻿namespace D2MapEdit
{
    partial class ConvertWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fromLabel = new System.Windows.Forms.Label();
            this.pathFromTextBox = new System.Windows.Forms.TextBox();
            this.selectPathFromButton = new System.Windows.Forms.Button();
            this.buttonSelectPathTo = new System.Windows.Forms.Button();
            this.textBoxPathTo = new System.Windows.Forms.TextBox();
            this.toLabel = new System.Windows.Forms.Label();
            this.openButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.processFolderButton = new System.Windows.Forms.Button();
            this.mapInfoWidget1 = new NevedaarWidgets.MapInfoWidget();
            this.ProcessFolderTextBox = new System.Windows.Forms.TextBox();
            this.SelectProcessFolderButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.RichTextBox();
            this.singleGroupBox = new System.Windows.Forms.GroupBox();
            this.folderGroupBox = new System.Windows.Forms.GroupBox();
            this.resultFolderButton = new System.Windows.Forms.Button();
            this.singleGroupBox.SuspendLayout();
            this.folderGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.Location = new System.Drawing.Point(6, 13);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(88, 13);
            this.fromLabel.TabIndex = 17;
            this.fromLabel.Text = "Game path(from):";
            // 
            // pathFromTextBox
            // 
            this.pathFromTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathFromTextBox.Location = new System.Drawing.Point(100, 9);
            this.pathFromTextBox.Name = "pathFromTextBox";
            this.pathFromTextBox.Size = new System.Drawing.Size(559, 20);
            this.pathFromTextBox.TabIndex = 18;
            this.pathFromTextBox.Text = "E:\\Games\\Game\\globals";
            // 
            // selectPathFromButton
            // 
            this.selectPathFromButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPathFromButton.Location = new System.Drawing.Point(665, 8);
            this.selectPathFromButton.Name = "selectPathFromButton";
            this.selectPathFromButton.Size = new System.Drawing.Size(29, 23);
            this.selectPathFromButton.TabIndex = 19;
            this.selectPathFromButton.Text = "...";
            this.selectPathFromButton.UseVisualStyleBackColor = true;
            this.selectPathFromButton.Click += new System.EventHandler(this.selectPathFromButton_Click);
            // 
            // buttonSelectPathTo
            // 
            this.buttonSelectPathTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectPathTo.Location = new System.Drawing.Point(665, 36);
            this.buttonSelectPathTo.Name = "buttonSelectPathTo";
            this.buttonSelectPathTo.Size = new System.Drawing.Size(29, 23);
            this.buttonSelectPathTo.TabIndex = 22;
            this.buttonSelectPathTo.Text = "...";
            this.buttonSelectPathTo.UseVisualStyleBackColor = true;
            this.buttonSelectPathTo.Click += new System.EventHandler(this.buttonSelectPathTo_Click);
            // 
            // textBoxPathTo
            // 
            this.textBoxPathTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPathTo.Location = new System.Drawing.Point(100, 37);
            this.textBoxPathTo.Name = "textBoxPathTo";
            this.textBoxPathTo.Size = new System.Drawing.Size(559, 20);
            this.textBoxPathTo.TabIndex = 21;
            this.textBoxPathTo.Text = "E:\\Games\\Game\\globals";
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Location = new System.Drawing.Point(6, 41);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(77, 13);
            this.toLabel.TabIndex = 20;
            this.toLabel.Text = "Game path(to):";
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(6, 19);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(225, 23);
            this.openButton.TabIndex = 25;
            this.openButton.Text = "OpenMap";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(6, 406);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(225, 23);
            this.saveButton.TabIndex = 26;
            this.saveButton.Text = "SaveMap";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // processFolderButton
            // 
            this.processFolderButton.Location = new System.Drawing.Point(6, 45);
            this.processFolderButton.Name = "processFolderButton";
            this.processFolderButton.Size = new System.Drawing.Size(228, 35);
            this.processFolderButton.TabIndex = 0;
            this.processFolderButton.Text = "ProcessFolder";
            this.processFolderButton.UseVisualStyleBackColor = true;
            this.processFolderButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // mapInfoWidget1
            // 
            this.mapInfoWidget1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapInfoWidget1.Location = new System.Drawing.Point(6, 45);
            this.mapInfoWidget1.Margin = new System.Windows.Forms.Padding(0);
            this.mapInfoWidget1.Name = "mapInfoWidget1";
            this.mapInfoWidget1.Size = new System.Drawing.Size(225, 356);
            this.mapInfoWidget1.TabIndex = 24;
            // 
            // ProcessFolderTextBox
            // 
            this.ProcessFolderTextBox.Location = new System.Drawing.Point(6, 19);
            this.ProcessFolderTextBox.Name = "ProcessFolderTextBox";
            this.ProcessFolderTextBox.Size = new System.Drawing.Size(199, 20);
            this.ProcessFolderTextBox.TabIndex = 27;
            this.ProcessFolderTextBox.Text = "E:\\Games\\Game\\globals";
            // 
            // SelectProcessFolderButton
            // 
            this.SelectProcessFolderButton.Location = new System.Drawing.Point(211, 19);
            this.SelectProcessFolderButton.Name = "SelectProcessFolderButton";
            this.SelectProcessFolderButton.Size = new System.Drawing.Size(23, 20);
            this.SelectProcessFolderButton.TabIndex = 28;
            this.SelectProcessFolderButton.Text = "...";
            this.SelectProcessFolderButton.UseVisualStyleBackColor = true;
            this.SelectProcessFolderButton.Click += new System.EventHandler(this.SelectProcessFolderButton_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.logTextBox.Location = new System.Drawing.Point(3, 74);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(201, 495);
            this.logTextBox.TabIndex = 29;
            this.logTextBox.Text = "";
            // 
            // singleGroupBox
            // 
            this.singleGroupBox.Controls.Add(this.openButton);
            this.singleGroupBox.Controls.Add(this.mapInfoWidget1);
            this.singleGroupBox.Controls.Add(this.saveButton);
            this.singleGroupBox.Location = new System.Drawing.Point(210, 74);
            this.singleGroupBox.Name = "singleGroupBox";
            this.singleGroupBox.Size = new System.Drawing.Size(237, 435);
            this.singleGroupBox.TabIndex = 30;
            this.singleGroupBox.TabStop = false;
            this.singleGroupBox.Text = "Single Map";
            // 
            // folderGroupBox
            // 
            this.folderGroupBox.Controls.Add(this.resultFolderButton);
            this.folderGroupBox.Controls.Add(this.processFolderButton);
            this.folderGroupBox.Controls.Add(this.ProcessFolderTextBox);
            this.folderGroupBox.Controls.Add(this.SelectProcessFolderButton);
            this.folderGroupBox.Location = new System.Drawing.Point(454, 74);
            this.folderGroupBox.Name = "folderGroupBox";
            this.folderGroupBox.Size = new System.Drawing.Size(240, 142);
            this.folderGroupBox.TabIndex = 31;
            this.folderGroupBox.TabStop = false;
            this.folderGroupBox.Text = "Folder";
            // 
            // resultFolderButton
            // 
            this.resultFolderButton.Location = new System.Drawing.Point(6, 108);
            this.resultFolderButton.Name = "resultFolderButton";
            this.resultFolderButton.Size = new System.Drawing.Size(228, 28);
            this.resultFolderButton.TabIndex = 29;
            this.resultFolderButton.Text = "Open result folder";
            this.resultFolderButton.UseVisualStyleBackColor = true;
            this.resultFolderButton.Click += new System.EventHandler(this.resultFolderButton_Click);
            // 
            // ConvertWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderGroupBox);
            this.Controls.Add(this.singleGroupBox);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.buttonSelectPathTo);
            this.Controls.Add(this.textBoxPathTo);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.selectPathFromButton);
            this.Controls.Add(this.pathFromTextBox);
            this.Controls.Add(this.fromLabel);
            this.Name = "ConvertWidget";
            this.Size = new System.Drawing.Size(704, 581);
            this.singleGroupBox.ResumeLayout(false);
            this.folderGroupBox.ResumeLayout(false);
            this.folderGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.TextBox pathFromTextBox;
        private System.Windows.Forms.Button selectPathFromButton;
        private System.Windows.Forms.Button buttonSelectPathTo;
        private System.Windows.Forms.TextBox textBoxPathTo;
        private System.Windows.Forms.Label toLabel;
        private NevedaarWidgets.MapInfoWidget mapInfoWidget1;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button processFolderButton;
        private System.Windows.Forms.TextBox ProcessFolderTextBox;
        private System.Windows.Forms.Button SelectProcessFolderButton;
        private System.Windows.Forms.RichTextBox logTextBox;
        private System.Windows.Forms.GroupBox singleGroupBox;
        private System.Windows.Forms.GroupBox folderGroupBox;
        private System.Windows.Forms.Button resultFolderButton;
    }
}
