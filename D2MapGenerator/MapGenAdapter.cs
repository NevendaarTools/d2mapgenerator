﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NevendaarTools.Transform;
using RandomStackGenerator;
using NevendaarTools;
using NevendaarTools.DataTypes;
using Point = RandomStackGenerator.Point;
using System.IO;
using static RandomStackGenerator.GenDefaultValues;
using System.Globalization;

namespace D2MapEdit
{
    //class MapGenAdapter : OptionsStorage
    //{
    //    public StackGenAdapter _Adapter;
    //    public SettingsManager _Settings = new SettingsManager("generation_settings.xml");
    //    RandomStackGenerator.ImpenetrableMeshGen _meshGen = new ImpenetrableMeshGen();
    //    public MapCreator _mapCreator = new MapCreator();
    //    public delegate void LogDelegate(string text);
    //    public LogDelegate _LogDelegate;
    //    public void Log(string str)
    //    {
    //        _LogDelegate?.Invoke(str);
    //    }
    //    //Point[] _CapPos;
    //    //string[] _MapLords;
    //    int[] _TreesAmont;
    //    TemplateForge forge;

    //    List<Option> options = new List<Option>();

    //    ImpenetrableMeshGen genmesh = new ImpenetrableMeshGen();
    //    ArrayZoom zoom = new ArrayZoom();
    //    ColorSelector draw = new ColorSelector();
    //    SymmetryOperations symm = new SymmetryOperations();
    //    StackPowerGen stackstats = new StackPowerGen();
    //    RaceGen racegen = new RaceGen();
    //    WaterGen watergenerator = new WaterGen();
    //    ImpenetrableObjects objplace;// = new ImpenetrableObjects();
    //    PenetrableObjectsGen penOnjGen = new PenetrableObjectsGen();
    //    ImpenetrableObjects.GlobalMapDecoration[] objectsSize;
    //    List<string> templatePaths = new List<string>();
    //    ImpenetrableMeshGen.GenSettings gsettings;
    //    public MapGenAdapter()
    //    {
    //        List<string> templates = new List<string>();
    //        string defaultTemplate = "";
    //        string[] s;
    //        templatePaths.Clear();
    //        s = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + ".\\Resources\\", "*template*.txt");
    //        foreach (string s2 in s)
    //        {
    //            templatePaths.Add(s2);
    //            string name = System.IO.Path.GetFileNameWithoutExtension(s2);
    //            templates.Add(name.Replace("template_", "").Replace("_template", "").Replace("_template", "").Replace(".txt", ""));
    //        }
    //        if (templates.Count > 0)
    //        {
    //            defaultTemplate = templates[0];

    //            Option presetOpt = Option.EnumOption("Template", _Settings.Value("template", defaultTemplate), "Template for map generation", templates);
    //            presetOpt.group = "Map";
    //            options.Add(presetOpt);

    //            int presetIndex = presetOpt.variants.IndexOf(presetOpt.value);
    //            string preset = templatePaths[presetIndex];
    //            if (File.Exists(preset))
    //            {
    //                gsettings = ImpenetrableMeshGen.GenSettings.Read(ref preset);
    //            }
    //            else
    //                _mapCreator.Log("File not exist = " + preset);

    //        }
    //        Option guardsOpt = Option.BoolOption("CapitalGuards", _Settings.Value("CapitalGuards", "True") == "True", "If enabled - place capital guard unit(Mizrael...)");
    //        guardsOpt.group = "Map";
    //        options.Add(guardsOpt);
    //        TextLanguage lang = TextLanguage.Eng;
    //        if (TranslationHelper.Instance().lang() == "ru")
    //        {
    //            lang = TextLanguage.Rus;
    //        }
    //        forge = new TemplateForge(ref lang);
    //    }

    //    public void PostInit()
    //    {

    //    }

    //    void FillTrees()
    //    {
    //        int maxRace = 0;
    //        foreach (int rCount in _Adapter._GameModel._ResourceModel.trees.Keys)
    //        {
    //            if (maxRace < rCount)
    //                maxRace = rCount;
    //        }

    //        _TreesAmont = new int[maxRace + 1];
    //        for (int i = 0; i <= maxRace; i++)
    //        {
    //            if (_Adapter._GameModel._ResourceModel.trees.ContainsKey(i))
    //                _TreesAmont[i] = _Adapter._GameModel._ResourceModel.trees[i];
    //            else
    //                _TreesAmont[i] = 0;
    //        }
    //    }

    //    void fillDecorations()
    //    {
    //        int size = _Adapter._GameModel._LMarks.Values.Count;
    //        size += _Adapter._GameModel._ResourceModel.mages.Count;
    //        size += _Adapter._GameModel._ResourceModel.mercs.Count;
    //        size += _Adapter._GameModel._ResourceModel.merhs.Count;
    //        size += _Adapter._GameModel._ResourceModel.ruins.Count;
    //        size += _Adapter._GameModel._ResourceModel.trainers.Count;
    //        size += _Adapter._GameModel._ResourceModel.mountains.Count;
    //        objectsSize = new ImpenetrableObjects.GlobalMapDecoration[size];

    //        int i = 0;
    //        foreach (GameLMark lmark in _Adapter._GameModel._LMarks.Values)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.objId.ToUpper(), Size = new Size(lmark.sizeX, lmark.sizeY) };
    //        }

    //        foreach (string lmark in _Adapter._GameModel._ResourceModel.mages)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.ToUpper(), Size = new Size(3, 3) };
    //        }

    //        foreach (string lmark in _Adapter._GameModel._ResourceModel.mercs)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.ToUpper(), Size = new Size(3, 3) };
    //        }
    //        foreach (string lmark in _Adapter._GameModel._ResourceModel.merhs)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.ToUpper(), Size = new Size(3, 3) };
    //        }
    //        foreach (string lmark in _Adapter._GameModel._ResourceModel.ruins)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.ToUpper(), Size = new Size(3, 3) };
    //        }
    //        foreach (string lmark in _Adapter._GameModel._ResourceModel.trainers)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = lmark.ToUpper(), Size = new Size(3, 3) };
    //        }
    //        foreach (GameResourceModel.Mountain mountain in _Adapter._GameModel._ResourceModel.mountains)
    //        {
    //            objectsSize[i++] = new ImpenetrableObjects.GlobalMapDecoration() { ID = mountain.name, Size = new Size(mountain.size, mountain.size) };
    //        }
    //    }

    //    public void Process()
    //    {

    //        //Call comm.ReadExcludedObjectsList({ "%default%"})
    //        //Dim objSizeArray() As ImpenetrableObjects.GlobalMapDecoration = ReadObjSize()

    //        AllDataStructues.Spell[] spells = new AllDataStructues.Spell[_Adapter._GameModel._Spells.Count];
    //        int i = 0;
    //        foreach (GameSpell spell in _Adapter._GameModel._Spells.Values)
    //        {
    //            AllDataStructues.Spell resSpell = new AllDataStructues.Spell();
    //            resSpell.name = spell.name;
    //            resSpell.category = spell.category;
    //            resSpell.level = spell.level;
    //            resSpell.spellID = spell.spellID;
    //            resSpell.castCost = AllDataStructues.Cost.Read(spell.castCost);
    //            resSpell.area = spell.area;
    //            resSpell.researchCost = new Dictionary<string, AllDataStructues.Cost>();

    //            foreach (string key in spell.researchCost.Keys)
    //            {
    //                resSpell.researchCost.Add(key.ToUpper(), AllDataStructues.Cost.Read(spell.researchCost[key]));
    //            }

    //            spells[i++] = resSpell;
    //        }

    //        fillDecorations();
    //        FillTrees();

    //        string[] desc = new string[1] { "%default%" };
    //        objplace = new ImpenetrableObjects(ref objectsSize, ref _Adapter.excluded, ref _Adapter.customRace, ref desc, ref spells);

    //        int genTimeLimit = 5000;

    //        //'### можно внести какие-то ихменения в шаблон типа размера карты, количества леса/воды и т.д.
    //        //If Not UseTemplateCheckBox.Checked Then
    //        //    Dim races As Integer
    //        //    If sel1.Checked Then
    //        //        races = 2
    //        //    ElseIf sel2.Checked Then
    //        //        races = 3
    //        //    Else
    //        //        races = 4
    //        //    End If
    //        //    gsettings.common_settMap.nRaces = races
    //        //    gsettings.common_settMap.ApplySymmetry = SymmCheckBox.Checked
    //        //    gsettings.common_settMap.SymmetryClass = -1
    //        //End If
    //        //gsettings.common_settMap.RoadsAmount = 0
    //        //gsettings.common_settMap.WaterAmount = 0
    //        //gsettings.common_settMap.ForestAmount = 0
    //        //'###
    //        int size = gsettings.common_settMap.xSize;
    //        //gsettings.common_settMap.xSize = size;
    //        //gsettings.common_settMap.ySize = size;
    //        gsettings.common_settMap.AddGuardsBetweenLocations = false;
    //        MapGenWrapper wrapper = new MapGenWrapper(ref objplace);
    //        Map mapRes = wrapper.CommonGen(ref gsettings, genTimeLimit);
    //        _mapCreator.Log(mapRes.log.PrintAll());

    //        gsettings.common_settMap.xSize = size;
    //        gsettings.common_settMap.ySize = size;
    //        bool usePlayable = true;
    //        TextLanguage lang = TextLanguage.Eng;
    //        if (TranslationHelper.Instance().lang() == "ru")
    //        {
    //            lang = TextLanguage.Rus;
    //        }
    //        shortMapFormat map = shortMapFormat.MapConversion(ref mapRes, ref gsettings, ref objectsSize,
    //            ref _Adapter._ContentSet,
    //            ref gsettings.common_settMap.ApplySymmetry,
    //            ref usePlayable, ref _TreesAmont, ref lang);
    //        _mapCreator.Log("size = " + (map.landscape.Length / size).ToString() + "x" + size.ToString());

    //        if (map.landscape.Length == size * size)
    //        {
    //            Generate(ref map, size);
    //        }
    //        else
    //        {

    //        }
    //    }

    //    private static int SubraceToRace(int num)
    //    {
    //        switch (num)
    //        {
    //            case 1: return 0;
    //            case 2: return 1;
    //            case 3: return 2;
    //            case 4: return 3;
    //            case 5: return 4;
    //            case 14: return 5;
    //        }
    //        return -1;
    //    }

    //    private void Generate(ref shortMapFormat map, int size)
    //    {
    //        //test();
    //        //return;
    //        MapGenSettings settings = new MapGenSettings();
    //        settings.size = size;
    //        _mapCreator.Create(settings);
    //        for (int i = 0; i < size; i++)
    //        {
    //            for (int k = 0; k < size; k++)
    //            {
    //                shortMapFormat.TileState tile = map.landscape[i, k];
    //                if (tile.ground == shortMapFormat.TileState.GroundType.Water)
    //                {
    //                    _mapCreator._grid[i, k].value = (int)D2MapBlock.TileType.Water;
    //                }
    //                else if (tile.ground == shortMapFormat.TileState.GroundType.Forest)
    //                {
    //                    Int32 race = (Int32)D2MapBlock.TileType.Neutrals;
    //                    if (tile.owner == "U")
    //                        race = (Int32)D2MapBlock.TileType.Undeads;
    //                    else if (tile.owner == "H")
    //                        race = (Int32)D2MapBlock.TileType.Empire;
    //                    else if (tile.owner == "L")
    //                        race = (Int32)D2MapBlock.TileType.Demons;
    //                    else if (tile.owner == "C")
    //                        race = (Int32)D2MapBlock.TileType.Dwarfs;
    //                    else if (tile.owner == "E")
    //                        race = (Int32)D2MapBlock.TileType.Elves;

    //                    _mapCreator._grid[i, k].value = (int)D2MapBlock.TileType.Tree | race |
    //                        tile.treeID << 24;
    //                }
    //            }
    //        }
    //        bool generateGuards = Option.GetBoolOption(ref options, "CapitalGuards");
    //        for (int i = 0; i < map.capitals.Count(); i++)
    //        {
    //            shortMapFormat.CapitalObject capital = map.capitals[i];
    //            List<string> items = new List<string>();
    //            foreach (var item in capital.items)
    //            {
    //                items.Add(item.itemID.ToUpper());
    //            }
    //            _mapCreator.AddPlayer(ref settings, capital.owner.byGame,
    //                capital.pos.X, capital.pos.Y,
    //                capital.lord, items, _Adapter._GameModel, capital.lordName, capital.objectName, generateGuards);
    //        }

    //        if (true)
    //        {
    //            FillVillages(ref map);
    //            FillMountains(ref map);
    //            FillLandmarks(ref map);
    //            FillStacks(ref map);
    //            FillMines(ref map, ref _Adapter._GameModel._ResourceModel.mines);
    //            FillRuins(ref map);
    //            FillMerchants(ref map);
    //            FillMercs(ref map);
    //            FillTrainers(ref map);
    //            FillMages(ref map);
    //        }

    //        _mapCreator.Finish();
    //    }

    //    private void FillMages(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.merchantsSpells.Count(); i++)
    //        {
    //            shortMapFormat.MerchantSpellObject mage = map.merchantsSpells[i];
    //            List<string> spells = new List<string>();
    //            for (int k = 0; k < mage.content.Length; ++k)
    //            {
    //                spells.Add(mage.content[k].spellID);
    //            }

    //            _mapCreator.AddMage(mage.pos.X, mage.pos.Y, int.Parse(mage.id.Substring(14)), ref spells, mage.objectName, mage.objectDescription);
    //        }
    //    }

    //    private void FillTrainers(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.trainers.Count(); i++)
    //        {
    //            shortMapFormat.TrainerObject trainer = map.trainers[i];
    //            _mapCreator.AddTrainer(trainer.pos.X, trainer.pos.Y, int.Parse(trainer.id.Substring(14)), trainer.objectName, trainer.objectDescription);
    //        }
    //    }

    //    private void FillMercs(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.merchantsUnits.Count(); i++)
    //        {
    //            shortMapFormat.MerchantUnitObject merch = map.merchantsUnits[i];
    //            List<D2Mercs.MercsUnitEntry> items = new List<D2Mercs.MercsUnitEntry>();
    //            for (int k = 0; k < merch.content.Length; ++k)
    //            {
    //                AllDataStructues.Unit unit = merch.content[k];
    //                items.Add(new D2Mercs.MercsUnitEntry() { _UnitId = unit.unitID, _UnitLevel = unit.level });
    //            }
    //            _mapCreator.AddMercs(merch.pos.X, merch.pos.Y, int.Parse(merch.id.Substring(14)), ref items, merch.objectName, merch.objectDescription);
    //        }
    //    }

    //    private void FillMerchants(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.merchantsItems.Count(); i++)
    //        {
    //            shortMapFormat.MerchantItemObject merch = map.merchantsItems[i];
    //            List<D2Merchant.MerchantItemEntry> items = new List<D2Merchant.MerchantItemEntry>();
    //            List<string> itemIds = new List<string>();
    //            for (int k = 0; k < merch.content.Length; ++k)
    //            {
    //                itemIds.Add(merch.content[k].itemID);
    //            }
    //            while (itemIds.Count > 0)
    //            {
    //                string item = itemIds[0];
    //                int count = itemIds.FindAll((string val) => { return item == val; }).Count;
    //                items.Add(new D2Merchant.MerchantItemEntry() { _ItemId = item, _ItemCount = count });
    //                itemIds.RemoveAll((string val) => { return item == val; });
    //            }

    //            _mapCreator.AddMerchant(merch.pos.X, merch.pos.Y, int.Parse(merch.id.Substring(14)), ref items, merch.objectName, merch.objectDescription);
    //        }
    //    }

    //    private void FillRuins(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.ruins.Count(); i++)
    //        {
    //            shortMapFormat.RuinObject ruin = map.ruins[i];
    //            StackTemplate inside = null;
    //            if (ruin.internalStack.pos != null)
    //            {
    //                _Adapter.SetStackName(ref ruin.internalStack);
    //                inside = _Adapter.TemplateFromGenStack(ruin.internalStack);
    //            }

    //            _mapCreator.AddRuin(ref _Adapter._GameModel,
    //                ruin.pos.X, ruin.pos.Y, inside,
    //                AllDataStructues.Cost.Print(ruin.resourcesReward),
    //                int.Parse(ruin.id.Substring(6)), ruin.objectName, "", ruin.ItemReward());
    //        }
    //    }

    //    private void FillMines(ref shortMapFormat map, ref List<string> mines)
    //    {
    //        for (int i = 0; i < map.mines.Count(); i++)
    //        {
    //            shortMapFormat.simpleObject mine = map.mines[i];
    //            _mapCreator.AddMine(mine.pos.X, mine.pos.Y, mines.IndexOf(mine.id));
    //        }
    //    }

    //    private void FillVillages(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.cities.Count(); i++)
    //        {
    //            shortMapFormat.CityObject village = map.cities[i];
    //            StackTemplate hero = null;
    //            if (village.exteternalStack.pos != null)
    //            {
    //                _Adapter.SetStackName(ref village.exteternalStack);
    //                hero = _Adapter.TemplateFromGenStack(village.exteternalStack);
    //            }

    //            StackTemplate inside = null;
    //            if (village.internalStack.pos != null)
    //            {
    //                _Adapter.SetStackName(ref village.internalStack);
    //                inside = _Adapter.TemplateFromGenStack(village.internalStack);
    //            }

    //            _mapCreator.AddVillage(ref _mapCreator._map, ref _Adapter._GameModel,
    //                village.pos.X, village.pos.Y, village.level, hero, inside, village.objectName);
    //        }
    //    }

    //    private void FillStacks(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.stacks.Count(); i++)
    //        {
    //            shortMapFormat.StackObject stack = map.stacks[i];
    //            if (stack.stack.pos != null)
    //            {
    //                _Adapter.SetStackName(ref stack.stack);
    //                StackTemplate hero = _Adapter.TemplateFromGenStack(stack.stack);
    //                _mapCreator.AddStack(ref _mapCreator._map, ref _Adapter._GameModel, hero, stack.pos.X, stack.pos.Y);

    //            }
    //        }
    //    }

    //    private void FillMountains(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.mountains.Count(); i++)
    //        {
    //            shortMapFormat.simpleObject mountain = map.mountains[i];
    //            int index = int.Parse(mountain.id.Substring(mountain.id.Length - 2));
    //            D2Mountains.Mountain mount = new D2Mountains.Mountain()
    //            {
    //                x = mountain.pos.X,
    //                y = mountain.pos.Y,
    //                sizeX = mountain.size.Width,
    //                sizeY = mountain.size.Height,
    //                image = index
    //            };
    //            _mapCreator.AddMountain(mount);
    //        }
    //    }

    //    private void FillLandmarks(ref shortMapFormat map)
    //    {
    //        for (int i = 0; i < map.landmarks.Count(); i++)
    //        {
    //            shortMapFormat.simpleObject landmark = map.landmarks[i];
    //            _mapCreator.AddLandMark(ref _mapCreator._map, ref _Adapter._GameModel, landmark.id, landmark.pos.X, landmark.pos.Y);
    //        }
    //    }

    //    public int OptionsCount()
    //    {
    //        int result = options.Count;
    //        for (int i = 0; i < forge.blocks.Count(); ++i)
    //        {
    //            result += forge.blocks[i].OptionsCount();
    //        }
    //        return result;
    //    }

    //    public int SetOption(int index, string value, ref bool neadReload)
    //    {
    //        if (index < options.Count)
    //        {
    //            options[index].value = value;
    //            neadReload = false;
    //            Option opt = options[index];
    //            if (opt.name == "Template")
    //            {
    //                int presetIndex = opt.variants.IndexOf(opt.value);
    //                string preset = templatePaths[presetIndex];
    //                _Settings.SetValue("template", opt.value);
    //                if (!File.Exists(preset))
    //                {
    //                    _mapCreator.Log("File not exist = " + preset);
    //                    return 1;
    //                }
    //                gsettings = ImpenetrableMeshGen.GenSettings.Read(ref preset);
    //                neadReload = true;
    //                return 0;
    //            }
    //        }
    //        else
    //        {
    //            int optIndex = options.Count;
    //            for (int i = 0; i < forge.blocks.Count(); ++i)
    //            {
    //                TemplateForge.OptionsStorage storage = forge.blocks[i];
    //                for (int k = 0; k < storage.OptionsCount(); ++k)
    //                {
    //                    if (index == optIndex)
    //                    {
    //                        TemplateForge.Parameter param = storage.GetOption(k);
    //                        if (param.name == "ReadFromFile")
    //                        {
    //                            FileInfo fileInfo = new FileInfo(value);
    //                            value = fileInfo.Name;
    //                        }
    //                        forge.SetValue(ref i, ref param.name, ref value);
    //                        param = storage.GetOption(k);
    //                        neadReload = false;
    //                        return 0;
    //                    }
    //                    optIndex++;
    //                }
    //            }
    //        }

    //        return 0;
    //    }

    //    public Option GetOptionAt(TemplateForge.OptionsStorage storage, int index)
    //    {
    //        TemplateForge.Parameter param = storage.GetOption(index);
    //        Option opt = new Option();
    //        string value = (param.valueLowerBound != null) ? param.valueLowerBound : "0";
    //        value = value.Replace(",", ".");
    //        value = value.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

    //        switch (param.type)
    //        {
    //            case TemplateForge.Parameter.ValueType.vBoolean:
    //                {
    //                    opt = Option.BoolOption(param.name, value == "True", param.description);
    //                }
    //                break;
    //            case TemplateForge.Parameter.ValueType.vDouble:
    //                opt = Option.FloatOption(param.name, value, param.description,
    //                    param.minValue, param.maxValue);
    //                break;
    //            case TemplateForge.Parameter.ValueType.vInteger:

    //                opt = Option.IntOption(param.name, value, param.description,
    //                    param.minValue, param.maxValue);
    //                break;
    //            case TemplateForge.Parameter.ValueType.vStringArray:
    //                {
    //                    List<string> trNames = new List<string>();
    //                    foreach (string val in param.possibleArrayValues[1])
    //                        trNames.Add(TranslationHelper.Instance().Tr(val));
    //                    opt = Option.SemgaOption(param.name, param.possibleArrayValues[0][0], param.description,
    //                        trNames, param.possibleArrayValues[1].ToList());
    //                    break;
    //                }
    //            case TemplateForge.Parameter.ValueType.vString:
    //                opt = Option.SelectPathOption(param.name, value, param.description, "Txt|*.txt;", System.IO.Directory.GetCurrentDirectory() + ".\\Resources\\");
    //                break;
    //            default:
    //                break;
    //        }
    //        //if (param.hidden)
    //        //    opt.status = Option.Status.Hiden;
    //        opt.group = storage.name;
    //        return opt;
    //    }

    //    public Option GetOptionAt(int index)
    //    {
    //        if (index < options.Count)
    //            return options[index];
    //        int optIndex = options.Count;
    //        for (int i = 0; i < forge.blocks.Count(); ++i)
    //        {
    //            TemplateForge.OptionsStorage storage = forge.blocks[i];
    //            for (int k = 0; k < storage.OptionsCount(); ++k)
    //            {
    //                if (index == optIndex)
    //                {
    //                    return GetOptionAt(storage, k);
    //                }
    //                optIndex++;
    //            }
    //        }
    //        return null;
    //    }
    //}
}
