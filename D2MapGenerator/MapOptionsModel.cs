﻿using NevendaarTools;
using NevendaarTools.MapUtils;
using NevendaarTools.Transform;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace D2MapEdit
{
    public class OptionsPreset
    {
        public class OptionValue
        {
            public string name;
            public string group;
            public string value;
        }
        public string name;
        public string desc = "";
        public List<OptionValue> values = new List<OptionValue>();
    }
    public class MapOptionsModel : OptionsStorage
    {
        private List<Option> options = new List<Option>();
        private MapEditor modificationHelper;
        public delegate void LogDelegate(string text);
        public LogDelegate _LogDelegate;
        private Dictionary<string, OptionsPreset> presets = new Dictionary<string, OptionsPreset>();
        private string dir;

        public void Log(string str)
        {
            _LogDelegate?.Invoke(str);
        }

        MapModel mapModel;

        public MapOptionsModel(MapEditor helper)
        {
            modificationHelper = helper;
        }

        public Option GetOptionAt(int index)
        {
            return options[index];
        }

        public int OptionsCount()
        {
            return options.Count;
        }

        public void Clear()
        {
            presets.Clear();
            options.Clear();
        }

        public int SetOption(int index, string value, ref bool neadReload)
        {
            options[index].value = value;
            neadReload = false;
            if (options[index].name == "Preset:")
            {
                if (!presets.ContainsKey(value))
                    return 0;
                OptionsPreset preset = presets[value];
                foreach(OptionsPreset.OptionValue val in preset.values)
                {
                    for(int i = 1; i < options.Count; ++i)
                    {
                        if (options[i].name == val.name && options[i].group == val.group)
                        {
                            SetOption(i, val.value, ref neadReload);
                            break;
                        }
                    }
                }
                neadReload = true;
                return 0;
            }
            int resValue = 0;
            if (options[index].type == Option.Type.Enum)
            {
                int varIndex = options[index].variants.IndexOf(value);
                if (varIndex < 0)
                    return 1;
                if (options[index].variantValues != null)
                    resValue = int.Parse(options[index].variantValues[varIndex]);
                else
                    resValue = int.Parse(options[index].variants[varIndex]);
            }
            else if (options[index].type == Option.Type.Int)
            {
                resValue = int.Parse(value);
            }
            else if (options[index].type == Option.Type.Float)
            {
                resValue = (int)float.Parse(value);
            }
            else if (options[index].type == Option.Type.Bool)
            {
                resValue = value == "True"? 1 : 0;
            }
            else
                return 1;

            if (modificationHelper.SetVariable(ref mapModel, options[index].name, resValue))
            {
            }
            else
            {
                Log("Failed to set variable value: " + options[index].name + " " +  resValue.ToString());
            }

            return 0;
        }

        public bool ReadOptions(ref MapModel map, string path)
        {
            options.Clear();
            dir = path;
            LoadPresets();
            if (presets.Count > 0)
            {
                Option option = Option.EnumOption("Preset:", "", "", new List<string>());
                option.group = "Load preset";
                foreach (var key in presets.Keys)
                {
                    option.variants.Add(presets[key].name);
                }
                options.Add(option);
            }
            mapModel = map;
            JSONObject json = JsonHelper.Load(path + "/options.json");
            if (json == null)
                return false;
            SimpleJSON.JSONArray groups = json["groups"].AsArray;
            foreach (JSONObject inObject in groups)
            {
                string group = inObject["group"];
                SimpleJSON.JSONArray groupOptions = inObject["options"].AsArray;
                foreach (JSONObject inOption in groupOptions)
                {
                    Option option = JsonHelper.OptionFromJson(inOption);
                    option.group = group;

                    string mapValue = modificationHelper.GetVariable(ref map, option.name);
                    if (mapValue != null)
                    {
                        if (option.variantValues != null && option.variantValues.Contains(mapValue))
                        {
                            option.value = option.variants[option.variantValues.IndexOf(mapValue)];
                        }
                        else
                            option.value = mapValue;
                    }
                    options.Add(option);
                }
            }
            
            return true;
        }

        private bool LoadPreset(string name, string filename)
        {
            JSONObject json = JsonHelper.Load(filename);
            if (json == null)
                return false;
            OptionsPreset preset = new OptionsPreset();
            preset.name = name;
            foreach(JSONObject record in json["options"].AsArray)
            {
                OptionsPreset.OptionValue val = new OptionsPreset.OptionValue();
                val.name = record["name"];
                val.group = record["group"];
                val.value = record["value"];
                preset.values.Add(val);
            }
            if (preset.values.Count > 0)
                presets.Add(name, preset);

            return true;
        }

        private void LoadPresets()
        {
            presets.Clear();
            try
            {
                IEnumerable<string> allfiles = Directory.EnumerateFiles(dir, "preset_*");
                foreach (string filename in allfiles)
                {
                    string name = System.IO.Path.GetFileNameWithoutExtension(filename);
                    name = name.Replace("preset_", "").Replace(".json","");
                    LoadPreset(name, filename);
                }
            }
            catch (Exception e)
            {

            }

        }
    }
}
