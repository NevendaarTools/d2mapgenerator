﻿using System;
using System.Collections.Generic;
using System.Text;
using RandomStackGenerator;
using NevendaarTools;
using System.IO;
using NevendaarTools.Transform;
using NevendaarTools.DataTypes;
using static NevendaarTools.DataTypes.D2Mercs;
using static RandomStackGenerator.GenDefaultValues;
using static RandomStackGenerator.RandStack;
using NevendaarTools.MapUtils;
using static RandomStackGenerator.AllDataStructues.Stack;

namespace D2MapEdit
{
    public class StackGenAdapter : MapObjectRegenerator, OptionsStorage
    {
        public OptionsStorage Options() { return this; }

        private MapReader _MapReader = new MapReader();
        private GameModel _GameModel = new GameModel();
        private MapEditor _Editor;

        //public MapModel _MapModel = new MapModel();

        public RandStack _Randomizer;
        public ObjectsContentSet _ContentSet;
        RandomStackGenerator.SetName _NameHelper;// = new SetName();
        private bool _NewMap = true;

        public float _PowerMult = 1.0f;
        int _PowerScatter = 0;
        int _ItemsScatter = 0;
        public float _ItemCostMult = 1.0f;
        bool _Debug = false;
        public bool _ExcludePlayableRace;
        public List<string> _ExcludedIds = new List<string>();

        Dictionary<string, AllDataStructues.Stack> _Namings = new Dictionary<string, AllDataStructues.Stack>();

        public void RegisterNaming(string id, AllDataStructues.Stack stack)
        {
            _Namings.Add(id, stack);
        }

        AllDataStructues.Item[] items;
        AllDataStructues.Unit[] units;
        AllDataStructues.Spell[] spells = new AllDataStructues.Spell[0];
        AllDataStructues.Modificator[] modificators;

        Point[] _CapPos;
        string[] _MapLords;
        AllDataStructues.Cost _MinesAmount;

        Dictionary<int, AllDataStructues.Restriction> _TypeCostRestriction = null;

        public Point[] CapPos { get => _CapPos; set => _CapPos = value; }
        public string[] MapLords { get => _MapLords; set => _MapLords = value; }
        public AllDataStructues.Cost MinesAmount { get => _MinesAmount; set => _MinesAmount = value; }
        public MapEditor Editor { get => _Editor; set => _Editor = value; }

        List<Option> options = new List<Option>();

        public void AddOption(Option option)
        {
            option.group = "Generator";
            options.Add(option);
        }

        public StackGenAdapter()
        {
            string[] mods = GenDefaultValues.GetSupportedMods();
            List<string> modsList = new List<string>();
            modsList.AddRange(mods);
            List<Option> defaultOptions = new List<Option>()
            {
                Option.BoolOption("Debug", false),
                Option.EnumOption("Modification", mods[0], "Modification_desc", modsList),
                Option.FloatOption("StackStrengthMultiplier", 1.0f),
                Option.IntOption("PowerScatter", 10, 0, 90),
                Option.FloatOption("UnitsStrengthUniformity", 0f, -100f, 100f),
                Option.BoolOption("ExcludePlayableRaceUnits", true),
                Option.BoolOption("ExcludeLoreUnits", true),
                Option.BoolOption("IgnoreStackRaces", false),
                Option.BoolOption("Overlevels", true),
                Option.FloatOption("LootCostMultiplier", 1.0f),
                Option.IntOption("ItemCostScatter", 10, 0, 90),
                Option.BoolOption("SaveItemTypes", false)
            };
            foreach (Option opt in defaultOptions)
                AddOption(opt);
        }
        public void Configure(MapEditor editor)
        {
            _Editor = editor;
            _NewMap = true;
            _Debug = Option.GetBoolOption(options, "Debug");
            _PowerMult = Option.GetFloatOption(options, "StackStrengthMultiplier");
            _ItemCostMult = Option.GetFloatOption(options, "LootCostMultiplier");
            _ExcludePlayableRace = Option.GetBoolOption(options, "ExcludePlayableRaceUnits");
            _ItemsScatter = Option.GetIntOption(options, "ItemCostScatter");
            _PowerScatter = Option.GetIntOption(options, "PowerScatter");

            GVars vars = _GameModel.GetAllT<GVars>()[0];

            ConstructorInput data = new ConstructorInput();
            data.AllItemsList = items;
            data.AllSpellsList = spells;
            data.AllUnitsList = units;
            data.AllModificatorsList = modificators;
            data.settings = new ConstructorInput.SettingsInfo();
            data.settings.unitsStrengthUniformity = Option.GetFloatOption(options, "UnitsStrengthUniformity");
            //data.settings.neutralOrderWeight
            data.settings.addUnitsFromBranchesToStacks = Option.GetBoolOption(options, "ExcludePlayableRaceUnits");
            data.settings.applyStrictTypesFilter = Option.GetBoolOption(options, "SaveItemTypes"); ;
            data.settings.excludeLoreUnits = Option.GetBoolOption(options, "ExcludeLoreUnits");
            data.settings.preserveLoreUnits = Option.GetBoolOption(options, "ExcludeLoreUnits");
            data.settings.ignoreUnitRace = Option.GetBoolOption(options, "IgnoreStackRaces");
            data.settings.modName = Option.GetOption(options, "Modification").value;
            
            data.settings.preserveUnitsOverlevel = Option.GetBoolOption(options, "Overlevels");
            data.settings.talismanChargesDefaultAmount = vars.talis_chrg;

            data.mapData = new ConstructorInput.MapInfo();
            data.mapData.capitalPos = _CapPos;
            data.mapData.mapLords = _MapLords;
            data.mapData.minesAmount = _MinesAmount;
            
            _Randomizer = new RandStack(ref data);
            _ContentSet = new ObjectsContentSet(ref _Randomizer);

            //_Randomizer.comm.ReadExcludedObjectsList();
            Logger.Log("_ExcludedIds count = " + _ExcludedIds.Count);
            if (_Debug)
            {
                _Randomizer.log.Enable();
                _NameHelper.log.Enable();
                Logger.Log("Generator stack power mult= " + _PowerMult.ToString());
                Logger.Log("Generator loot cost mult = " + _ItemCostMult.ToString());
                string excludedStr = "Excluded: ";
                for (int i = 0; i < _ExcludedIds.Count; ++i)
                {
                    excludedStr += " " + _ExcludedIds[i];
                }
                Logger.Log(excludedStr);
                _Randomizer.comm.PrintResourcesToLog(ref _Randomizer.log, ref _Randomizer);
            }
            else
            {
                _NameHelper.log.Disable();
                _Randomizer.log.Disable();
            }
        }

        public void Finish(MapModel map)
        {
            if (_Debug)
            {
                Logger.Log(_Randomizer.log.PrintAll());
                Logger.Log(_NameHelper.log.PrintAll());
            }
            AllDataStructues.Stack[] stacks = new AllDataStructues.Stack[_Namings.Count];
            string [] ids = new string[_Namings.Count];
            int i = 0;
            foreach(string id in _Namings.Keys)
            {
                ids[i] = id;
                stacks[i++] = _Namings[id];
            }
            _NameHelper.GenName(ref stacks, ref _Randomizer, _NewMap);

            for(int k = 0; k < i; ++k)
            {
                _Namings[ids[k]] = stacks[k];
            }
            List<D2StackTemplate> objects = map.GetListByType<D2StackTemplate>();
            foreach (D2StackTemplate stack in objects)
            {
                if (_Namings.ContainsKey(stack._ObjId) && _Namings[stack._ObjId].name != null)
                {
                    stack._Name = _Namings[stack._ObjId].name;
                }
                else if (stack._Name == null)
                {
                    Logger.LogError("Empty name for stack template:" + stack._ObjId);
                    stack._Name = "NAME_ERROR";
                }
                map.Replace(stack);
            }
            List<D2Stack> mapstacks = map.GetListByType<D2Stack>();
            foreach (D2Stack stack in mapstacks)
            {
                D2Unit leader = map.GetById<D2Unit>(stack._leaderId);

                if (_Namings.ContainsKey(stack._objId) && _Namings[stack._objId].name != null)
                {
                    leader._Name = _Namings[stack._objId].name;
                }
                else if (leader._Name == null)
                {
                    Logger.LogError("Empty name for stack:" + stack._objId + "("
                        + stack._posX.ToString() + " : " + stack._posY.ToString() + ")");
                    Gunit unit = _GameModel.GetObjectT<Gunit>(leader._Type);
                    leader._Name = unit.name_txt.value.text;
                }
                map.Replace(leader);
                map.Replace(stack);
            }
            _Namings.Clear();
        }

        public AllDataStructues.Stack GenStackFromTemplate(StackTemplate template)
        {
            AllDataStructues.Stack genStack = new AllDataStructues.Stack
            {
                units = new UnitInfo[6],
                items = new List<string>()
            };
            LOrder order = _GameModel.GetObjectT<LOrder>(template.order);
            LOrder orderAI = _GameModel.GetObjectT<LOrder>(template.aiOrder);
            genStack.order = new StackOrder(order.text, template.orderTarget, orderAI.text, template.aiOrderTarget);

            genStack.leaderPos = template._LeaderIndex;

            List<string> used = new List<string>();
            for (int index = 0; index < 6; index++)
            {
                if (template._Pos[index] == -1)
                {
                    genStack.units[index] = new UnitInfo("G000000000",
                        1, null, ref _Randomizer);
                }
                else
                {
                    if (template._Pos[index] == template._LeaderIndex)
                        genStack.leaderPos = index;

                    Gunit unit = _GameModel.GetObjectT<Gunit>(template._Unit[template._Pos[index]]);

                    if (used.Contains(template._Unit[template._Pos[index]]) && !unit.size_small)
                    {
                        genStack.units[index] = new UnitInfo("G000000000",
                        1, null, ref _Randomizer);
                        continue;
                    }

                    used.Add(template._Unit[template._Pos[index]]);
                    genStack.units[index] = new UnitInfo( template._Unit[template._Pos[index]],
                        template._UnitLvl[template._Pos[index]], template._Mods[template._Pos[index]], ref _Randomizer);
                    genStack.units[index].modificators = template._Mods[template._Pos[index]];
                }
            }
            foreach (string itemid in template._Items)
                genStack.items.Add(itemid);

            genStack.name = template.name;

            return genStack;
        }

        public StackTemplate TemplateFromGenStack(AllDataStructues.Stack stack)
        {
            StackTemplate result = new StackTemplate();

            for (int unitIndex = 0; unitIndex < 6; unitIndex++)
            {
                result._Unit[unitIndex] = "G000000000";
                result._Pos[unitIndex] = -1;
                result._UnitLvl[unitIndex] = 0;
            }
            result._LeaderIndex = stack.leaderPos;


            int index = 0;
            for (int i = 0; i < 6; i++)
            {
                if (stack.units[i] != null && stack.units[i].unit.unitID != "G000000000")
                {
                    Gunit unit = _GameModel.GetObjectT<Gunit>(stack.units[i].unit.unitID);
                    int level = Math.Max(1, unit.level);

                    result._Unit[index] = stack.units[i].unit.unitID;
                    result._UnitLvl[index++] = level;
                    result._Pos[i] = index - 1;
                    result._Mods[result._Pos[i]] = stack.units[i].modificators;

                    if (!unit.size_small)
                        if (i % 2 == 0)
                            result._Pos[i + 1] = index - 1;
                        else
                            result._Pos[i - 1] = index - 1;

                    if (i == stack.leaderPos || (!unit.size_small && (i + 1) == stack.leaderPos))
                    {
                        result._LeaderIndex = index - 1;
                    }
                }
            }
            result.name = stack.name;
            foreach (string itemid in stack.items)
                result._Items.Add(itemid);

            List<LOrder> orders = _GameModel.GetAllT<LOrder>();
            foreach(LOrder order in orders)
            {
                if (order.text == stack.order.order.name)
                {
                    result.order = order.id;
                    result.orderTarget = stack.order.order.target;
                    break;
                }
            }
            foreach (LOrder order in orders)
            {
                if (order.text == stack.order.aiOrder.name)
                {
                    result.aiOrder = order.id;
                    result.aiOrderTarget = stack.order.aiOrder.target;
                    break;
                }
            }

            return result;
        }

        public StackTemplate RegenerateStack(ref StackTemplate template, float extraPowerMult = 1.0f, float extraItemsMult = 1.0f)
        {
            List<string> questItems = new List<string>();

            AllDataStructues.Stack genStack = GenStackFromTemplate(template);
            AllDataStructues.DesiredStats stats = _Randomizer.StackStats(ref genStack, false);

            bool ground = Enums.TileGround((UInt32)( _Editor.grid[template.mapX, template.mapY].value)) != (uint)Enums.GroundType.WATER;
            
            bool withLeader = genStack.leaderPos != -1;
            int extraLeaderShip = 0;
            if (template._LeaderIndex != -1)
            {
                foreach (string modId in template.LeaderMods())
                {
                    if (modId == "G000UM9031")
                        extraLeaderShip++;
                    else
                        if (modId == "G000UM9032")
                        extraLeaderShip--;
                }
            }
            Random r = new Random(DateTime.Now.Millisecond);
            float powerValue = _PowerMult * (1 + (r.Next(0, _PowerScatter * 2) - _PowerScatter) / 100.0f);
            float itemsValue = _ItemCostMult * (1 + (r.Next(0, _ItemsScatter * 2) - _ItemsScatter) / 100.0f);
            Point point = new Point(template.mapX, template.mapY);
            AllDataStructues.CommonStackCreationSettings genSettings = new AllDataStructues.CommonStackCreationSettings();
            genSettings.noLeader = !withLeader;
            genSettings.groundTile = ground;
            genSettings.isTemplate = template.template;
            genSettings.ownerIsPlayableRaсe = template.playableOwner;
            
            LOrder order = _GameModel.GetObjectT<LOrder>(template.order);
            LOrder orderAI = _GameModel.GetObjectT<LOrder>(template.aiOrder);
            genSettings.order = new StackOrder(order.text, template.orderTarget, orderAI.text, template.aiOrderTarget);

            genSettings.deltaLeadership = extraLeaderShip;
            genSettings.pos = point;
            genSettings.StackStats = stats;
            AllDataStructues.Stack resultStack = _Randomizer.Gen(ref genSettings, powerValue * extraPowerMult, itemsValue * extraItemsMult);

            StackTemplate resultTemplate = TemplateFromGenStack(resultStack);
            //if (withLeader)
            //    resultTemplate.SetLeaderMods(template.LeaderMods());

            foreach (string itemId in questItems)
            {
                resultTemplate._Items.Add(itemId);
            }
            if (template.extraData != "")
            {
                RegisterNaming(template.extraData, resultStack);
            }
            return resultTemplate;
        }

        public List<D2Merchant.MerchantItemEntry> RegenerateMerchantItem(ref List<D2Merchant.MerchantItemEntry> old, int mode)
        {
            List<D2Merchant.MerchantItemEntry> result = new List<D2Merchant.MerchantItemEntry>();

            List<string> items = new List<string>();
            for (int i = 0; i < old.Count; ++i)
            {
                for (int k = 0; k < old[i]._ItemCount; k++)
                    items.Add(old[i]._ItemId);
            }
            AllDataStructues.DesiredStats stats = new AllDataStructues.DesiredStats();
            stats.shopContent = _ContentSet.GetMerchantListSettings(mode, ref items);
            stats.IGen = _Randomizer.GetItemsGenSettings(ref items, false);
            stats.IGen.JewelItems.exclude = true;
            List<string> newItems = _ContentSet.MakeMerchantItemsList(ref stats, ref _TypeCostRestriction, ref _Randomizer.log);

            while (newItems.Count > 0)
            {
                string item = newItems[0];
                int count = newItems.FindAll((string val) => { return item == val; }).Count;
                result.Add(new D2Merchant.MerchantItemEntry() { _ItemId = item, _ItemCount = count });
                newItems.RemoveAll((string val) => { return item == val; });
            }

            return result;
        }

        public List<MercsUnitEntry> RegenerateMercsUnits(ref List<MercsUnitEntry> old, int mode)
        {
            List<MercsUnitEntry> result = new List<MercsUnitEntry>();
            AllDataStructues.DesiredStats stats = new AllDataStructues.DesiredStats();
            string[] units = new string[old.Count];
            for (int i = 0; i < old.Count; ++i)
            {
                units[i] = old[i]._UnitId;
            }

            stats.shopContent = _ContentSet.GetMercenariesListSettings(mode, ref units);
            List<string> newUnits = _ContentSet.MakeMercenariesList(ref stats, ref _Randomizer.log);

            foreach (string unit in newUnits)
            {
                Gunit gunit = _GameModel.GetObjectT<Gunit>(unit);
                int level = Math.Max(1, gunit.level);
                result.Add(new MercsUnitEntry() { _UnitId = unit, _UnitLevel = level, _UnitUniq = false });
            }

            return result;
        }


        public RuinReward RegenerateRuinReward(string item, string cash, bool addItemCostDelta, int x, int y, float manaChance, float manaValue)
        {
            GItem gameItem = _GameModel.GetObjectT<GItem>(item);

            if (gameItem.item_cat == 14)//quest item
                return new RuinReward() { item = item, cash = cash };

            List<string> tmp = new List<string>() { item };
            AllDataStructues.LootGenSettings settings = _Randomizer.GetItemsGenSettings(ref tmp, true);

            AllDataStructues.Cost itemCost = _Randomizer.LootCost(item);
            Point point = new Point(x, y);
            AllDataStructues.CommonLootCreationSettings genSettings = new AllDataStructues.CommonLootCreationSettings();
            genSettings.GoldCost = itemCost.Gold;
            genSettings.IGen = settings;
            genSettings.pos = point;
            genSettings.TypeCostRestriction = _TypeCostRestriction;
            string newItem = _Randomizer.ThingGen(genSettings, 1.0);
            if (addItemCostDelta)
            {
                AllDataStructues.Cost newItemCost = AllDataStructues.Cost.Read("G0000");
                if (newItem != "")
                    newItemCost = _Randomizer.LootCost(newItem);
                AllDataStructues.Cost deltaCost = itemCost - newItemCost;
                AllDataStructues.Cost cashCost = AllDataStructues.Cost.Read(cash) + deltaCost;
                cashCost = _Randomizer.GoldToMana(ref cashCost, manaChance, manaValue);

                return new RuinReward() { item = newItem, cash = AllDataStructues.Cost.Print(cashCost) };
            }
            return new RuinReward() { item = newItem, cash = cash };
        }

        public string RegenerateItem(string itemId, int x, int y)
        {
            GItem gameItem = _GameModel.GetObjectT<GItem>(itemId);

            if (gameItem.item_cat == 14)
                return itemId;

            List<string> tmp = new List<string>() { itemId };
            AllDataStructues.LootGenSettings settings = _Randomizer.GetItemsGenSettings(ref tmp, false);
            AllDataStructues.Cost cost = _Randomizer.LootCost(ref items);
            int goldSum = AllDataStructues.Cost.Sum(cost);
            Point point = new Point(x, y);
            AllDataStructues.CommonLootCreationSettings genSettings = new AllDataStructues.CommonLootCreationSettings();
            genSettings.GoldCost = goldSum;
            genSettings.IGen = settings;
            genSettings.pos = point;
            genSettings.TypeCostRestriction = _TypeCostRestriction;
            return _Randomizer.ThingGen(genSettings, 1.0);
        }

        public string GenerateItem(string cost, int x, int y, float mod = 0.5f)
        {
            AllDataStructues.LootGenSettings settings = new AllDataStructues.LootGenSettings(false)
            {
                JewelItems = new AllDataStructues.ItemGenSettings()
                {
                    exclude = true
                }
            };

            string itemCash = cost.Substring(1, 4);
            Point point = new Point(x, y);
            AllDataStructues.CommonLootCreationSettings genSettings = new AllDataStructues.CommonLootCreationSettings();
            genSettings.GoldCost = (int)(int.Parse(itemCash) * mod);
            genSettings.IGen = settings;
            genSettings.pos = point;
            genSettings.TypeCostRestriction = _TypeCostRestriction;
            return _Randomizer.ThingGen(genSettings, 1.0);
        }

        public List<string> RegenerateItems(ref List<string> items, int x, int y)
        {
            List<string> result = new List<string>();
            List<string> questItems = new List<string>();

            AllDataStructues.LootGenSettings settings = _Randomizer.GetItemsGenSettings(ref items, false);
            AllDataStructues.Cost cost = _Randomizer.LootCost(ref items);
            int goldSum = AllDataStructues.Cost.Sum(cost);
            Point point = new Point(x, y);
            AllDataStructues.CommonLootCreationSettings genSettings = new AllDataStructues.CommonLootCreationSettings();
            genSettings.GoldCost = goldSum;
            genSettings.IGen = settings;
            genSettings.pos = point;
            genSettings.TypeCostRestriction = _TypeCostRestriction;
         
            result = _Randomizer.ItemsGen(genSettings, _ItemCostMult);
            foreach (string itemId in questItems)
            {
                result.Add(itemId);
            }
            return result;
        }

        public void LoadGameData(GameModel gameModel)
        {
            _GameModel = gameModel;
            List<Gunit> gunits = _GameModel.GetAllT<Gunit>();
            units = new AllDataStructues.Unit[gunits.Count];

            int index = 0;
            foreach (Gunit unit in gunits)
            {
                GDynUpgr upgr1 = unit.dyn_upg1.value;
                GDynUpgr upgr2 = unit.dyn_upg2.value;
                units[index] = new AllDataStructues.Unit();
                
                units[index].EXPkilled = unit.xp_killed;
                units[index].EXPnext = unit.xp_next;
                units[index].unitCost = AllDataStructues.Cost.Read(unit.enroll_c);
                
                units[index].leadership = unit.leadership;
                units[index].level = unit.level;
                units[index].race = unit.subrace.key;
                units[index].reach = unit.attack_id.value.reach.key;
                if (units[index].reach > 3)
                {
                    units[index].reach = unit.attack_id.value.reach.value.melee ? 3 : 2;
                }
                units[index].small = unit.size_small;
                units[index].unitBranch = unit.branch.key;
                units[index].unitID = unit.unit_id;
                units[index].waterOnly = unit.water_only;
                units[index].name = unit.name_txt.value.text;

                units[index].dynUpgrade1.unitCost = AllDataStructues.Cost.Read(upgr1.enroll_c);
                units[index].dynUpgrade1.EXPnext = upgr1.xp_next;
                units[index].dynUpgrade1.EXPkilled = upgr1.xp_killed;
                units[index].dynUpgrade1.accuracy = upgr1.power;
                units[index].dynUpgrade1.hp = upgr1.hit_point;
                units[index].dynUpgrade1.initiative = upgr1.initiative;
                units[index].dynUpgrade1.heal = upgr1.heal;
                units[index].dynUpgrade1.damage = upgr1.damage;

                units[index].dynUpgrade2.unitCost = AllDataStructues.Cost.Read(upgr2.enroll_c);
                units[index].dynUpgrade2.EXPnext = upgr2.xp_next;
                units[index].dynUpgrade2.EXPkilled = upgr2.xp_killed;
                units[index].dynUpgrade2.accuracy = upgr2.power;
                units[index].dynUpgrade2.hp = upgr2.hit_point;
                units[index].dynUpgrade2.initiative = upgr2.initiative;
                units[index].dynUpgrade2.heal = upgr2.heal;
                units[index].dynUpgrade2.damage = upgr2.damage;
                units[index].dynUpgradeLevel = unit.dyn_upg_lv;

                Gattack attack = unit.attack_id.value;
                units[index].hp = unit.hit_point;
                units[index].armor = unit.armor;
                units[index].damage = attack.qty_dam;
                units[index].heal = attack.qty_heal;
                units[index].accuracy = attack.power;
                units[index].initiative = attack.initiative;
                units[index].ASourceImmunity = new Dictionary<int, int>();
                units[index].AClassImmunity = new Dictionary<int, int>();

                foreach (Gimmu immu in unit.immun.value)
                {
                    if (!units[index].ASourceImmunity.ContainsKey(immu.immunity.key))
                    {
                        units[index].ASourceImmunity.Add(immu.immunity.key, immu.immunecat.key);
                    }
                }
                foreach (GimmuC immu in unit.immunCategory.value)
                {
                    if (!units[index].AClassImmunity.ContainsKey(immu.immunity.key))
                    {
                        units[index].AClassImmunity.Add(immu.immunity.key, immu.immunecat.key);
                    }
                }

                index++;
            }

            index = 0;
            List<GItem> gitems = _GameModel.GetAllT<GItem>();
            items = new AllDataStructues.Item[gitems.Count];
            foreach (GItem item in gitems)
            {
                items[index] = new AllDataStructues.Item();
                AllDataStructues.Item resultItem = items[index];
                items[index].itemCost = AllDataStructues.Cost.Read(item.value);
                items[index].name = item.name_txt.value.text;
                items[index].type = (GenDefaultValues.ItemTypes)item.item_cat;
                items[index].itemID = item.item_id;
                index++;
            }

            index = 0;
            List<Gspell> gspells = _GameModel.GetAllT<Gspell>();
            spells = new AllDataStructues.Spell[gspells.Count];
            foreach (Gspell item in gspells)
            {
                spells[index] = new AllDataStructues.Spell();
                AllDataStructues.Spell resultItem = spells[index];
                spells[index].castCost = AllDataStructues.Cost.Read(item.casting_c);
                spells[index].name = item.name_txt.value.text;
                spells[index].level = item.level;
                spells[index].spellID = item.spell_id.ToUpper();
                spells[index].category = item.category;
                spells[index].area = item.area;
                spells[index].researchCost = new Dictionary<string, AllDataStructues.Cost>();
                foreach (GspellR lord in item.researchCost.value)
                {
                    spells[index].researchCost.Add(lord.lord_id.ToUpper(), AllDataStructues.Cost.Read(lord.research));
                }
                index++;
            }

            index = 0;
            List<Gmodif> gmodifs = _GameModel.GetAllT<Gmodif>();
            modificators = new AllDataStructues.Modificator[gmodifs.Count];
            foreach (Gmodif item in gmodifs)
            {
                modificators[index] = new AllDataStructues.Modificator();
                AllDataStructues.Modificator resultItem = modificators[index];
                modificators[index].id = item.modif_id;
                modificators[index].source = (AllDataStructues.Modificator.Type)item.source;
                modificators[index].effect = new List<AllDataStructues.Modificator.ModifEffect>();
                foreach(GmodifL effect in item.effects.value)
                {
                    AllDataStructues.Modificator.ModifEffect resEffect = new AllDataStructues.Modificator.ModifEffect();
                    resEffect.ability = effect.ability;
                    resEffect.immunAClass = effect.immunityc.key;
                    resEffect.immunAClassCat = (AllDataStructues.Modificator.ModifEffect.ImmunityCat)effect.immunecatc.key;
                    resEffect.immunASource = effect.immunity.key;
                    resEffect.immunASourceCat = (AllDataStructues.Modificator.ModifEffect.ImmunityCat)effect.immunecat.key;
                    resEffect.move = effect.move;
                    resEffect.number = effect.number;
                    resEffect.percent = effect.percent;
                    resEffect.type = (AllDataStructues.Modificator.ModifEffect.EffectType)effect.type;
                    modificators[index].effect.Add(resEffect);
                }
                index++;
            }

            GVars vars = _GameModel.GetAllT<GVars>()[0];

            ConstructorInput data = new ConstructorInput();
            data.AllItemsList = items;
            data.AllSpellsList = spells;
            data.AllUnitsList = units;
            data.AllModificatorsList = modificators;
            data.settings = new ConstructorInput.SettingsInfo();
            data.settings.unitsStrengthUniformity = Option.GetFloatOption(options, "UnitsStrengthUniformity");
            data.settings.addUnitsFromBranchesToStacks = Option.GetBoolOption(options, "ExcludePlayableRaceUnits");
            data.settings.applyStrictTypesFilter = Option.GetBoolOption(options, "SaveItemTypes"); ;
            data.settings.excludeLoreUnits = Option.GetBoolOption(options, "ExcludeLoreUnits");
            data.settings.ignoreUnitRace = Option.GetBoolOption(options, "IgnoreStackRaces");
            data.settings.modName = Option.GetOption(options, "Modification").value;
            data.settings.preserveUnitsOverlevel = Option.GetBoolOption(options, "Overlevels");
            data.settings.talismanChargesDefaultAmount = vars.talis_chrg;

            data.mapData = new ConstructorInput.MapInfo();
            data.mapData.capitalPos = _CapPos;
            data.mapData.mapLords = _MapLords;
            data.mapData.minesAmount = _MinesAmount;

            _Randomizer = new RandStack(ref data); _ContentSet = new ObjectsContentSet(ref _Randomizer);
            TextLanguage lang = TextLanguage.Eng;
            if (TranslationHelper.Instance().lang() == "ru")
            {
                lang = TextLanguage.Rus;
            }
            _NameHelper = new SetName(lang, Option.GetOption(options, "Modification").value);
        }

        public int OptionsCount()
        {
            return options.Count;
        }

        public int SetOption(int index, string value, ref bool neadReload)
        {
            options[index].value = value;
            return 0;
        }

        public Option GetOptionAt(int index)
        {
            return options[index];
        }
    }
}
